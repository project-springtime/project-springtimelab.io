---
title: "Project Springtime"
subtitle: "Open-source-style hydroponics"
class: "front"
---

{{< load-photoswipe >}}
{{< gallery dir="/systems/vert-rdwc-1/images-front/" />}}

Open-source [hydroponics](https://en.wikipedia.org/wiki/Hydroponics) in my backyard to
grow my own lettuce, herbs and other veggies, can that work? Read on.

<div class="leftright">
 <div class="left">
  <p>If:</p>
  <ul>
   <li>you have an unused, sunny spot in your backyard about 5ft by 5ft;</li>
   <li>you have ordinary homeowner handiman skills;
   <li>you have access to some tools like a drill and a power saw;
   <li>you can follow instructions :-)
   <li>you are willing spend about 200-400 dollars (depending on what tools you
       have already or which options you choose)
  </ul>
 </div>
 <div class="right">
  <p>then:</p>
  <ul>
   <li>you can probably build this compact, vertical hydroponics system with
       room for over 70 plants over the course of a weekend or two;</li>
   <li>you can harvest more lettuce on an ongoing basis than your family can
       possibly eat :-) but you could grow some other herbs and veggies as well;</li>
   <li>it uses a form a
       <a href="https://en.wikipedia.org/wiki/Deep_water_culture">deep water culture</a>
       that recirculates its water, to reduce the amount of work you need to do on an
       ongoing basis.</li>
  </ul>
 </div>
</div>

We document how to do that on this site, because we want everybody to be
able to grow (some of) their own food right in their backyard, without
needing to do a lot of research and experimentation first. &quot;Experience
is cheapest second-hand&quot; it is said, and if you build this system,
you get our experience for free.

In exchange, we encourage you to submit bug reports and
[improvements](https://docs.gitlab.com/ee/user/project/merge_requests/),
[open-source](https://en.m.wikipedia.org/wiki/Open-source_model) style,
so we (and everybody else) can benefit from your experience, too, and
the system gets even better over time. Seems fair?

Here is the content we have so far:

* {{% pageref "systems/vert-rdwc-1/" %}}:
  covers all the parts, where to get them from, how to cut / drill / assemble
  / etc them; also covers how to operate and maintain the system on an
  ongoing basis.
* {{% pageref "systems/vert-rdwc-2/" %}}:
  same, but for a larger system with about twice the footprint.
* {{% pageref "pests-and-diseases/" %}}:
  we are cataloging, with pictures, what pests and diseases we have encountered so far,
  and what we have done about them. With photos.
* {{% pageref "faq/" %}}
* {{% pageref "research/" %}}:
  collection of notes to understand the system, and related alternatives. You should not need
  to consult this to successfully setup and operate the system.

<style type="text/css">
#groupsio_embed_signup label { font-weight:bold;}
#groupsio_embed_signup .email { margin:0 10px; min-width:40%; }
</style>

<div id="groupsio_embed_signup">
 <form action="https://groups.io/g/project-springtime/signup?u=6658561173251000354" method="post" id="groupsio-embedded-subscribe-form" name="groupsio-embedded-subscribe-form" target="_blank">
  <div id="groupsio_embed_signup_scroll">
   <label for="email" id="templateformtitle">Subscribe to the mailing list:</label>
   <input type="email" value="" name="email" class="email" id="email" placeholder="email address" required="">
   <input type="submit" value="Subscribe" name="subscribe" id="groupsio-embedded-subscribe" class="button">
   <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6658561173251000354" tabindex="-1" value=""></div>
   <div id="templatearchives"></div>
   <p style="font-size: 70%; text-align: center">No spamming. Just hydroponicking. With (just a few, so far) friends.</p>
  </div>
 </form>
</div>
