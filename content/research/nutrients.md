---
title: Nutrients
---

## General

* Required composition of minerals differs from that for soils
* Most important: composition in water can change very rapidly eg. because
  of selective uptake by plants
* Concentration should be between 1000 and 2500 ppm

## About the water

Cornell strongly recommends to start with water analysis.

* Alkalinity, reported as units of Calcium Carbonate equivalents CaCO3.
  0 to 300 ppm is common. More important value than pH.

  * "Depending on your alkalinity, you may need to choose a fertilizer
    formulation with a greater proportion of acidic nitrogen forms
    (ammonium or urea) or add acid to neutralize the alkalinity and counter
    the pH rise."

  * "Often linked with your water alkalinity are considerable levels of
     Ca, Mg and S in the water", also Sodium and chloride and compensate
     with fertilizer.

* Electrical conductivity (EC), should ideally beless than 0.25 mS/cm for
  closed systems.


## Needed nutrients

### Macronutrients

* nitrogen
* phosphorus
* potassium
* sulfur
* calcium
* magnesium

### Micronutrients

* iron
* manganese
* zinc
* boron
* copper
* molybdenum
* chloride
* nickel

Chloride and nickel aren’t included in most recipes, as they’re available in sufficient
quantities as impurities.

## Plant types

For vegetative crops, most nutrient-solution recipes don’t adjust the ratio
of nutrients while they grow; whereas, in fruiting crops the ratio may be
adjusted to alter the shift between vegetative and reproductive growth.

* 150ppm N works well for head and leaf lettuce during the main production stage;
* 175 to 200ppm N is more appropriate for kale, Swiss chard and mustard greens,
  which tend to be slightly heavier feeders.
* During seedling propagation a some-what lower fertilizer target of 125ppm N
  works well for both
* tomato and other fruiting crops have higher nutrient demands than leafy greens:
  higher nitrogen, potassium, calcium and magnesium


## Plant stages

* vegetative growth: high in nitrogen, calcium and magnesium
* flowering: high in potassium and phosphorus
* nitrogen restriction inhibits vegetative growth and helps induce flowering.

## Recipies

* http://www.greenhouse.cornell.edu/crops/factsheets/hydroponic-recipes.pdf
* https://cals.arizona.edu/hydroponictomatoes/nutritio.htm

## Symptoms of nutrient deficiencies

* Nice table: https://www.hydroponics.net/learn/nutrient_deficiencies.php
  (copied to this site {{% pageref deficiencies.md here %}} for easier printing)
* Description: https://www.hydroponics.net/learn/deficiency_by_element.php
* https://cals.arizona.edu/hydroponictomatoes/nutritio.htm

## Improving nutrient uptake

To improve nutrient uptake:

* chelating agents can be added
  * humic substances (Note: science seems to know very little about humic substances.
    They are complex molecules apparently created during dead plant breakdown, which
    help with plant growth "substantially" according to some studies)

## Mixing

* Use stock tanks to avoid a nasty precipitate or sludge that will occur when
  specific nutrients are mixed in the concentrated form (e.g. calcium can combine
  with phosphates and sulfates to form insoluble precipitates)

## Sources

* https://en.wikipedia.org/wiki/Hydroponics
* https://en.wikipedia.org/wiki/Humic_substance
* https://www.hydroponics.net/learn/nutrient_deficiencies.php
* http://www.greenhouse.cornell.edu/crops/factsheets/hydroponic-recipes.pdf
* https://cals.arizona.edu/hydroponictomatoes/nutritio.htm
