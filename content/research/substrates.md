---
title: Substrates
---

## Expanded clay aggregate

* Suitable for hydroponic systems in which all nutrients are carefully controlled in water solution.
* re-usable because they can be cleaned and sterilized, typically by washing in:
  * solutions of white vinegar
  * chlorine bleach
  * hydrogen peroxide
  * and rinsing completely.
* but: best not re-used even when they are cleaned, due to root growth that may enter the medium.

## Growstones

* Made from glass waste
* more air and water retention space than perlite and peat
* holds more water than parboiled rice hulls.

## Coir peat

* also known as coir or coco
* colonized with trichoderma fungi, which protects roots and stimulates root growth
* extremely difficult to over-water
* high cation exchange, meaning it can store unused minerals to be released to the plant as and when
  it requires it.
* Coir is available in many forms

## Rice husks

## Perlite

* volcanic rock that has been superheated
* used loose or in plastic sleeves immersed in the water.
* similar properties and uses to vermiculite but, in general, holds more air and less water and is buoyant.

## Vermiculite

* mineral that has been superheated until it has expanded into light pebbles
* natural "wicking" property that can draw water and nutrients in a passive hydroponic system.
* If too much water and not enough air surrounds the plants roots, it is possible to gradually
  lower the medium's water-retention capability by mixing in increasing quantities of perlite.

## Pumice stone

## Sand

* does not hold water very well
* must be sterilized between uses

## Gravel

* same type that is used in aquariums
* any small gravel can be used, provided it is washed first.
* easy to keep clean, drains well and will not become waterlogged
* if the system does not provide continuous water, the plant roots may dry out.

## Wood fibre

* produced from steam friction of wood
* keeps its structure for a very long time
* wood fibre may have detrimental effects on "plant growth regulators".

## Sheep wool

* little-used yet promising
* greatest yield out of the tested substrates

## Rock wool

Pro:

* most widely used medium in hydroponics.
* typically used only for the seedling stage, or with newly cut clones
* can remain with the plant base for its lifetime
* can be engineered to hold large quantities of water and air
* good mechanical structure to hold the plant stable

Con:

* possible skin irritancy (mechanical) whilst handling (1:1000)
* naturally high pH of mineral wool makes them initially unsuitable to plant
  growth and requires "conditioning" to produce a wool with an appropriate,
  stable pH.

## Brick shards

* similar properties to gravel.

Added disadvantages:

* possibly altering the pH
* requiring extra cleaning before reuse.

## Polystyrene packing peanuts

Pro:

* inexpensive
* readily available
* excellent drainage
* used mainly in closed-tube systems
* non-biodegradable polystyrene peanuts must be used

Con:

* too lightweight for some uses
* biodegradable packing peanuts will decompose into a sludge
* Plants may absorb styrene and pass it to their consumers

## Sources

* https://en.wikipedia.org/wiki/Expanded_clay_aggregate
* https://en.wikipedia.org/wiki/Hydroponics
* https://en.wikipedia.org/wiki/Kratky_method
