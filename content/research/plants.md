---
title: Suitable Plants
---

## Commonly grown:

* tomatoes
* peppers
* cucumbers
* lettuces
* marijuana.

## Criteria

Non-suitable:

* Plants that require drying between waterings or a dry dormant period

## Sources

* https://en.wikipedia.org/wiki/Passive_hydroponics
