---
title: Potential Problems
---

## Organic hydroponics

Difficult.

## Root rot

Explanation:

* Excess water makes it very difficult for the roots to get the air that they need

Causes:

* poor drainage
* usually, this is a result of overwatering.
* heavy soil, such as one dug up from outdoors
* in hydroponic applications, if the water is not properly aerated
* Many cases of root rot are caused by members of the water mold genus Phytophthora;
  perhaps the most aggressive is P. cinnamomi. Spores from root rot causing agents
  do contaminate other plants, but the rot cannot take hold unless there is adequate
  moisture. Spores are not only airborne, but are also carried by insects and other
  arthropods in the soil.

Prevention:

* only water plants when the soil becomes dry
* put the plant in a well-drained pot
* Root rot and other problems associated with poor water aeration were principal reasons
  for the development of aeroponics.

Remedy:

* usually lethal and there is no effective treatment.

Sources:

* https://en.wikipedia.org/wiki/Root_rot
* https://en.wikipedia.org/wiki/Organic_hydroponics
