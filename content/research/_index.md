---
title: Research
comments: false
---

Collection of interesting factoids related to hydroponic growing,
with links to the sources of those factoids.
