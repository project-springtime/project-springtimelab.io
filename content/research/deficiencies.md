---
title: Nutrient deficiencies
---

<style>
table{
    text-align: center;
}
thead {
    font-weight: bold;
}
td {
    width: 50px;
}
td:first-child {
    text-align: left;
}
td.first {
    width: 250px;
}
</style>

<table id="deficiencies">
 <thead>
  <tr>
    <td rowspan="2">Symptoms</td>
    <td colspan="11">Suspected Element</td>
  </tr>
  <tr>
    <td>N</td>
    <td>P</td>
    <td>K</td>
    <td>Mg</td>
    <td>Fe</td>
    <td>Cu</td>
    <td>Zn</td>
    <td>B</td>
    <td>Mo</td>
    <td>Mn</td>
    <td>Over<br>
      Fertilization
   </tr>
  </thead>
  <tr>
    <td class="first">Yellowing of Younger leaves</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
  </tr>
  <tr>
    <td>Yellowing of Middle leaves</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Yellowing of Older leaves</td>
    <td>&#10004;</td>
    <td></td>
    <td>&#10004;</td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Yellowing Between veins</td>
    <td></td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
  </tr>
  <tr>
    <td>Old leaves drop</td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Leaf Curl Over</td>
    <td></td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Leaf Curl Under</td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
  </tr>
  <tr>
    <td>Leaf tips burn, Younger leaves</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Leaf tips burn, Older leaves</td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Young leaves wrinkle and curl</td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td>&#10004;</td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Dead areas in the leaves</td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td>&#10004;</td>
    <td>&#10004;</td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
  </tr>
  <tr>
    <td>Leaf growth stunted</td>
    <td>&#10004;</td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Dark green/purplish leaves and stems</td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Pale green leaf color</td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Leaf Spotting</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Spindly</td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Soft stems</td>
    <td>&#10004;</td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Hard/brittle stems</td>
    <td></td>
    <td>&#10004;</td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Growing tips die</td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Stunted root growth</td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Wilting</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>&#10004;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
