---
title: Hydroponic Methods
---

## Categories

* top irrigation
* sub-irrigation

## Note on aeration

1 kg of water can only hold 8 mg of air, no matter whether aerators are utilized or not.

Accomplished with:

* air pump
* air stones
* air diffusers
* adjustment of the frequency and length of watering cycles
* growth media that contain more air space than traditional potting mixes

Particularly important for:

* epiphytic plants such as orchids and bromeliads, whose roots are exposed to the air in nature.

## Static solution culture

### Kratky method

Approach:

* plants are placed in net cups filled with an inert [substrate](../substrates/)
* net cups are suspended above a reservoir of water containing essential
  nutrients in solution.
* Only the root tips are allowed to touch the surface of the reservoir.
* As the plant grows and depletes the water level, a gap of moist air will
  form and expand between the water surface and the base of the plant.
  The roots in this gap become laterally branching "oxygen roots," and
  absorb oxygen from the air inside the container.
* By the time the water level is fully depleted, the plant should be
  ready to harvest.

For:

* leafy vegetables that do not consume large amounts of water

Pros:

* passive
* no additional inputs of water or nutrients are needed after the original application
* no electricity, pumps, or water and oxygen circulation systems are required.

Cons:

* depending on root growth speed of plant, a mechanical contraption is required
  to keep the roots at the correct distance from the water surface, e.g by
  placing the net cups in boards that float on water, but rest on support beams
  "at the right time".

Assessment:

* very low-maintenance if it works
* requires trial and error for size of bucket, distances etc based on
  plant and climate

## Continuous-flow solution culture

Approach:

* nutrient solution constantly flows past the roots

Pros:

* much easier to automate than the static solution culture because sampling and
  adjustments to the temperature and nutrient concentrations can be made in a
  large storage tank that has potential to serve thousands of plants.

### Nutrient film technique (NFT)

Approach:

* very shallow stream of water containing all the dissolved nutrients is
  recirculated past the bare roots of plants in a watertight thick root mat,
  which develops in the bottom of the channel and has an upper surface that,
  although moist, is in the air. This provides an abundant supply of oxygen
  to the roots of the plants.

Needs:

* the right channel slope (so there are no ponds)
  * 1:100 but hard to do
  * 1:30 to 1:40 is easier
* the right flow rate
  * 1 liter/minute
  * at planting: 0.5 l / min
  * no higher than 2 l / min
* the right channel length
  * no longer than 12m, otherwise Nitrogen is depleted along the line

Pros:

* plant roots are exposed to adequate supplies of water, oxygen, and nutrients.
  In all other forms of production, there is a conflict between the supply
  of these requirements, since excessive or deficient amounts of one results
  in an imbalance of one or both of the others.
* higher yields of high-quality produce are obtained over an extended period
  of cropping.

Cons:

* very little buffering against interruptions in the flow (e.g., power outages).

## Aeroponics

* Maintenance-intense (keeping nozzles clean)
* Excellent aeration is the main advantage of aeroponics.
* Fine control over water vs air

Used for:

* propagation
* seed germination
* seed potato production
* tomato production
* leaf crops
* micro-greens

NASA research has shown that aeroponically grown plants have an 80% increase in dry weight biomass (essential minerals) compared to hydroponically grown plants. Aeroponics used 65% less water than hydroponics. NASA also concluded that aeroponically grown plants require ¼ the nutrient input compared to hydroponics.

Unlike hydroponically grown plants, aeroponically grown plants will not suffer transplant shock when transplanted to soil, and offers growers the ability to reduce the spread of disease and pathogens.

## Fogponics

* Even more complicated than Aeroponics

## Passive sub-irrigation

Approach:

* plants are grown in an inert porous medium that transports water and fertilizer
  to the roots by capillary action from a separate reservoir as necessary
* in the simplest method, the pot sits in a shallow solution of fertilizer
  and water or on a capillary mat saturated with nutrient solution.

The various hydroponic media available, such as expanded clay and coconut husk, contain more air space than more traditional potting mixes, delivering increased oxygen to the roots, which is important in epiphytic plants such as orchids and bromeliads, whose roots are exposed to the air in nature. Additional advantages of passive hydroponics are the reduction of root rot and the additional ambient humidity provided through evaporations.

## Ebb and flow (flood and drain) sub-irrigation

Approach:

* In its simplest form, there is a tray above a reservoir of nutrient solution.
  At regular intervals, a simple timer causes a pump to fill the tray with
  nutrient solution, after which the solution drains back down into the

## Run-to-waste

Approach:

* nutrient and water solution is periodically applied to the medium surface.
* in its simplest form, a nutrient-and-water solution is manually applied one
  or more times per day to a container of inert growing media
* in a slightly more complex system, it is automated with a delivery pump,
  a timer and irrigation tubing to deliver nutrient solution with a delivery
  frequency that is governed by the key parameters of plant size,
  plant growing stage, climate, substrate, and substrate conductivity, pH,
  and water content.

## Deep water culture

Approach:

* suspending the plant roots in a solution of nutrient-rich, oxygenated water.
* plant contained in a net pot suspended from the centre of the lid and the
  roots suspended in the nutrient solution.
* The solution is oxygen saturated by an air pump combined with porous stones.

## Top-fed deep water culture

Approach:

* delivering highly oxygenated nutrient solution direct to the root zone of
  plants.
* The water is released over the plant's roots and then runs back into the
  reservoir below in a constantly recirculating system
* airstone pumps air into the water via a hose
* airstone and the water pump run 24 hours a day.

Pros:

* The biggest advantage of top-fed deep water culture over standard deep water culture
  is increased growth during the first few weeks: the roots get easy access to water
  from the beginning. Grow time can be reduced by a few weeks.

## Rotary

Approach:

* circular frame which rotates continuously during the entire growth cycle
* typically rotate once per hour, no pause

Pros:

* due to the plants continuous fight against gravity, plants typically mature
  much more quickly than when grown in soil or other traditional hydroponic
  growing systems.
* Small foot print

## Dutch Bucket aka Bato Bucket

Approach:

* separate buckets
* single watering line, common exit
* easy to extend, flexible

## Sources:

* https://en.wikipedia.org/wiki/Hydroponics
* https://en.wikipedia.org/wiki/Kratky_method
* https://en.wikipedia.org/wiki/Nutrient_film_technique
* https://en.wikipedia.org/wiki/Passive_hydroponics
* https://en.wikipedia.org/wiki/Root_rot
* Rob Laing: Ditch the Dirt. Dovetail.
