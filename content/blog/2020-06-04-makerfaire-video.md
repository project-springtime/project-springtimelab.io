---
title: Video from Maker Faire is now available
date: 2020-06-04
---

The live walkthrough of Project Springtime from **Virtually Maker Faire** on May 23, 2020,
is now available on our new [YouTube channel](https://youtu.be/UYu12GwymDM).

{{% youtube UYu12GwymDM %}}
