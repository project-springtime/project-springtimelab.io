---
Title: Things are growing in January! With pictures.
date: 2021-01-16
---

Admittedly this is the San Francisco Bay Area in Northern California,
but this is an extraordinary warm winter. The plants are growing,
slower than in the summer, but growing!

Here are some pictures:

{{< load-photoswipe >}}
{{< gallery dir="/blog/2021-01-16/plants/" />}}

And the pests never went away:

{{< load-photoswipe >}}
{{< gallery dir="/blog/2021-01-16/pests/" />}}

Some plants didn't make it, like the Basil, and the Cucumbers.
But it is nice to be able to go out and grab something fresh for
dinner, or a little snack, even in January.

