---
title: '"I built one!" in New Zealand'
date: 2020-06-21
---

Community member James Smith
[announced](https://groups.io/g/project-springtime/topic/i_built_one/74998772?p=,,,20,0,0,0::recentpostdate%2Fsticky,,,20,2,0,74998772)
on the [mailing list](https://groups.io/g/project-springtime/) that he built a system
using Project Springtime. Here is a picture:

![New system](/systems/james-smith-1/images/hydro.jpg)

