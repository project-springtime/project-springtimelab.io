---
title: A new plants section ... and a new logo
date: 2020-07-22
---

Keeping with the idea of collecting best practices, stuff that worked and didn't,
I just added a section on {{% pageref "plants/" %}}. The idea is to catalogue which plants
work well in this system, and which don't, in the hope that the next grower won't
have to re-learn what others learned before them.

And there's a new logo: cherry tomatoes growing on a Project Springtime System.
Here is the large version:

![Project icon large](/img/project-icon-large.jpg)

