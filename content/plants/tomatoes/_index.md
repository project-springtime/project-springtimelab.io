---
Title: Tomatoes
Summary: Grow rapidly, but the plants are too large for this system
---

## Tomatoes and Project Springtime

* All varieties grow very rapidly and produce lots of fruit

## Issues

* Even with aggressive trimming, the plants are really too large for
  the {{% pageref "/systems/vert-rdwc-1/" %}} or the {{% pageref "/systems/vert-rdwc-2/" %}}.
  Roots may clog the pipes even before the first fruit are ripe.

## Varieties grown successfully

* Heirloom, Matt's wild cherry, Solanum lycopersicum var. cerasiforme, from Johnny's Selected Seeds
* Hybrid greenhouse, Geronimo F1, Solanum lycopersicum, from Johnny's Selected Seeds
* Heirloom, San Marzano II OG, Solanum lycopersicum, from Johnny's Selected Seeds

## Varieties currently growing, too early to tell

* Tomatillos, Toma Verde, Physalis philadelphica, from Johnny's Selected Seeds

## Varieties grown unsuccessfully

N/A

## Varieties not yet grown

* Heirloom, Cherokee Purple OG, Solanum lycopersicum, from Johnny's Selected Seeds

