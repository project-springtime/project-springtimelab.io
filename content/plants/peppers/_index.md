---
Title: Peppers
Summary: Growing well, space may be an issue.
---

## Peppers and Project Springtime

* Growing well.
* The plants may get taller than the distance to the next growing pipe above.

## Issues

N/A

## Varieties grown successfully

* Japaleno Mild, from Ferry Morse
* Sweet Pepper, California Wonder, from Burpee
* Hybrid Bell Peppers, X3R Red Knight F1, Capsicum annuum, from Johnny's Selected Seeds
* Ancho/Poblano Pepper Chile, from Botanical Interests
* Hybrid Hot Peppers, Charger F1, Capsicum annuum, from Johnny's Selected Seeds

## Varieties grown unsuccessfully

N/A

## Varieties not yet grown

N/A

