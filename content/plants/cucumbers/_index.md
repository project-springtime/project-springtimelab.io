---
Title: Cucumbers
Summary: They grow rapidly, produce great fruit, but rapidly outgrow the system.
---

## Cucumbers and Project Springtime

* All varieties grow very rapidly and produce lots of fruit

## Issues

* The plants grow roots that may become too large for
  the {{% pageref "/systems/vert-rdwc-1/" %}} or the {{% pageref "/systems/vert-rdwc-2/" %}}
  and clog the growing pipes.

## Varieties grown successfully

* Hybrid slicing, Corinto F1 OG, Cucumis sativus, from Johnny's Selected Seeds
* Hybrid slicing, Olympian F1, Cucumis sativus, from Johnny's Selected Seeds
* Cool customer OG, Cucumis sativus, from Johnny's Selected Seeds

## Varieties grown unsuccessfully

N/A

## Varieties not yet grown

N/A

