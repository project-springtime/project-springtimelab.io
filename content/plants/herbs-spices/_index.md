---
Title: Herbs and Spices
Summary: Mixed results.
---

## Spices and Project Springtime

Results depend on the plant.

## Issues

* Basil grows very nicely, but the roots can soon clog the pipes. Because the
  seeds are so small, germination hasn't always been successful.
* The cilantro and the parsley grow, but not much. It may not be worth it.

## Varieties grown successfully

* Genevese basil, from Seeds of Change
* Sweet basil, from Burpee
* Dill, from Seeds of Change

## Varieties grown unsuccessfully

* Cilantro, Santo, Coriandrum sativum, from Johnny's Selected Seeds
* Cilantro, from Ferry Morse
* Parsley, Giant of Italy OG, Petroselinum crispum, from Johnny's Selected Seeds
* Sage, broad leaf, from Ferry Morse

## Varieties not yet grown

* Mountain mint, Pyenanthemum pilosum, from Johnny's Selected Seeds
* Creeping thyme, Thymus serpyllum, from Johnny's Selected Seeds
* Common mint, Mentha spp, from Johnny's Selected Seeds
