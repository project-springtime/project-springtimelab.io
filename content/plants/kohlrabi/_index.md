---
Title: Kohlrabi
Summary: They do very well.
---

## Kohlrabi and Project Springtime

* They grow very well.

## Issues

* Some caterpillars like Kohlrabi leaves.

## Varieties grown successfully

* Hybrid kohlrabi, Terek F1, Brassica oleracea var gongylodes, from Johnny's Selected Seeds
* Kohlrabi, Early white vienna, from Ferry Morse

## Varieties grown unsuccessfully

N/A

## Varieties not yet grown

N/A

