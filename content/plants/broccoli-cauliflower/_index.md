---
Title: Broccoli and cauliflower
Summary: They take too much room.
---

## Broccoli and cauliflower and Project Springtime

* They grow very well.

## Issues

* Even with aggressive trimming, broccoli and cauliflower plants are really too large
  for the {{% pageref "/systems/vert-rdwc-1/" %}} or the {{% pageref "/systems/vert-rdwc-2/" %}}.
  Cauliflower and broccoli plants are really large (many leaves around the
  edible part). The corresponding root systems are large and easily clog the pipes even
  before the plants are mature.

## Varieties grown successfully

* Hybrid broccoli, Monflor F1, Brassica oleracea, from Johnny's Selected Seeds

## Varieties grown unsuccessfully

N/A

## Varieties not yet grown

* Heirloom cauliflower, snowball, from Seeds of Change

