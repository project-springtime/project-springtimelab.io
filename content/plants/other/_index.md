---
Title: Other types of plants
Summary: Mixed early results.
---

## Other types of plants and Project Springtime

Too early to tell.

## Varieties grown successfully

* Diploid watermelons, Sugar baby, Citrullus lanatus var. lanatus, from Johnny's Selected Seeds
* Celery, Tango OG, Apium graveolens, from Johnny's Selected Seeds
* Round red beets, Babybeat, Beta vulgaris, from Johnny's Selected Seeds. Take care to harvest
  in time so the beet doesn't get too large for the growing pot.

## Varieties grown unsuccessfully

* Radish, Champion. from Ferry Morse.
  While the plant grows just fine, the radish gets a wrinkly, relatively hard skin.
  Also, it's very inefficient use of space on this system as you cannot seed densely.

## Varieties currently growing, too early to tell

* Hybrid strawberry, Elan F1, Fragaria x ananassa, from Johnny's Selected Seeds.
  Some difficulties with germination. Maybe the seeds drown in the growing cubes.
* Rhubarb, Victoria, Rheum rhabarbarum, from Johnny's Selected Seeds

## Varieties not yet grown

* Onion, Cabernet, Allium cepa (hybrid), from Botanical Interests
* Hybrid mini eggplant, Hansel F1, Solanum melongena, from Johnny's Selected Seeds

