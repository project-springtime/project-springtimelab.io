---
Title: Lettuce
Summary: Easy to grow, in many varieties.
---

## Lettuce and Project Springtime

* Easiest plant to grow in this system
* Because lettuce is wide and not tall, it sits directly on the growing
  pipes and rarely needs extra support

## Issues

N/A

## Varieties grown successfully

* Romaine, Green Forest MT0, Lactuca sativa, from Johnny's Selected Seeds
* Butterhead, Mirlo OG MT0, Lactuca sativa, from Johnny's Selected Seeds
* Romaine, Montel Carlo MT0, Lactuca sativa, from Johnny's Selected Seeds
* Oakleaf, Rouxai MT0, Lactuca sativa, from Johnny's Selected Seeds
* Summer Crisp, Muir OG MT0, Lactuca sativa, from Johnny's Selected Seeds

## Varieties grown unsuccessfully

N/A

## Varieties not yet grown

N/A
