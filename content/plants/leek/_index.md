---
Title: Leeks
Summary: They do very well.
---

## Leeks and Project Springtime

* Leeks grow well

## Issues

N/A

## Varieties grown successfully

* Leek, American flag, from Ferry Morse
* Hybrid leek, Jumper F1, Allium porrum, from Johnny's Selected Seeds

## Varieties grown unsuccessfully

N/A

## Varieties not yet grown

N/A
