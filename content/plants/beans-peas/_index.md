---
Title: Beans and peas
Summary: They grow well, if you are creative where you let them climb.
---

## Beans and peas and Project Springtime

* Grow well
* Depending on variety, the plants may become very tall.
* Plants need a trellis or other support to hold on to.

## Issues

N/A

## Varieties grown successfully

* Fava beans, Vroma, Vicia faba, from Johnny's Selected Seeds
* Bush beans, Royal burgundy, Phaseolus vulgaris, from Johnny's Selected Seeds
* Bush beans, Annihilator, Phaseolus vulgaris, from Johnny's Selected Seeds
* Snap peas, Super sugersnap, Pisum sativum, from Johnny's Selected Seeds

## Varieties grown unsuccessfully

N/A

## Varieties not yet grown

N/A

