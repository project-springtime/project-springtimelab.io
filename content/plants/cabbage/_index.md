---
Title: Cabbage, Pac Choi and similar
Summary: So far, so good. No harvest yet.
---

## Cabbage and Project Springtime

So far, so good.

## Issues

N/A

## Varieties grown successfully

* Hybrid pac choi, Asian delight F1, Brassica rapa var. chinensis, from Johnny's Selected Seeds
* Escarole, Natacha, Cichorium endivia, from Johnny's Selected Seeds
* Radicchio, Bel Fiore, Cichorium intybus, from Johnny's Selected Seeds

## Varieties grown unsuccessfully

N/A

## Varieties currently growing, too early to tell

* Hybrid fresh market red, Omero F1, Brassica oleracea var. capitata, from Johnny's Selected Seeds
* Hybrid fresh market green, Tiara F1, Brassica oleracea var. capitata, from Johnny's Selected Seeds
* Hybrid brussels sprouts, Hestia F1, Brassica oleracea var. gemmifera, from Johnny's Selected Seeds
