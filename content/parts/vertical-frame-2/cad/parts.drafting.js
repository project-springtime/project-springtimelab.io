djsRender( document.currentScript, function( params ) {
    var p  = new DjsPoint( 0, 0 );

    var pFoot        = p.plus( 0, 72 );
    var pPlank       = p.plus( 0, 54 );
    var pPost        = p.plus( 0, 36 );
    var pCenterBrace = p.plus( 0, 18 );
    var pFootBrace   = p;

    return new DjsDiagram(
        [
            new DjsRectanglePart( pFoot, pFoot.plus( 48, 3.5 )),
            new DjsLinearMeasure( pFoot, pFoot.plus( 48, 0 ) ),
            new DjsLinearMeasure( pFoot.plus( 48, 0 ), pFoot.plus( 48, 3.5 )),
            new DjsPartLabel( pFoot.plus( 48, 3.5 ), "Foot (3x)" ),

            new DjsRectanglePart( pPlank, pPlank.plus( 96, 3.5 )),
            new DjsLinearMeasure( pPlank, pPlank.plus( 96, 0 ) ),
            new DjsLinearMeasure( pPlank.plus( 96, 0 ), pPlank.plus( 96, 3.5 )),
            new DjsPartLabel( pPlank.plus( 96, 3.5 ), "Horizontal Plank (2x)" ),

            new DjsRectanglePart( pPost, pPost.plus( 96, 3.5 )),
            new DjsLinearMeasure( pPost, pPost.plus( 96, 0 ) ),
            new DjsLinearMeasure( pPost.plus( 96, 0 ), pPost.plus( 96, 3.5 )),
            new DjsPartLabel( pPost.plus( 96, 3.5 ), "Post (3x)" ),

            new DjsRectanglePart( pCenterBrace, pCenterBrace.plus( 68, 3.5 )),
            new DjsLinearMeasure( pCenterBrace, pCenterBrace.plus( 68, 0 ) ),
            new DjsLinearMeasure( pCenterBrace.plus( 68, 0 ), pCenterBrace.plus( 68, 3.5 )),
            new DjsPartLabel( pCenterBrace.plus( 68, 3.5 ), "Center Brace (2x)" ),

            new DjsClosedPathPart( pFootBrace )
                    .lineTo( pFootBrace.plus( 22, 0 ))
                    .lineTo( pFootBrace.plus( 22 - 3.5, 3.5 ))
                    .lineTo( pFootBrace.plus( 3.5, 3.5 )),
            new DjsLinearMeasure( pFootBrace, pFootBrace.plus( 22, 0 ) ),
            new DjsLinearMeasure( pFootBrace.plus( 22, 0 ), pFootBrace.plus( 22, 3.5 )),
            new DjsLinearMeasure( pFootBrace.plus( 0, 3.5 ), pFootBrace.plus( 3.5, 3.5 ), 0 ),
            new DjsLinearMeasure( pFootBrace.plus( 22-3.5, 3.5 ), pFootBrace.plus( 22, 3.5 ), 0 ),
            new DjsPartLabel( pFootBrace.plus( 11, 3.5 ), "Foot Brace (6x)" ),
        ],
        params );
} );
