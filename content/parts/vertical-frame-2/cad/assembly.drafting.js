djsRender( document.currentScript, function( params ) {
    var p      = new DjsPoint( 0, 0 );

    var p1  = p;
    var p2  = p.plus( 48 - 1.75/2, 0 );
    var p22 = p.plus( 96 - 1.75, 0 );
    var p3  = p.plus( 0, 18 );
    var p4  = p.plus( 0, 72 );

    var braceAngle = 53 * Math.PI / 180;
    var braceDx    = Math.cos( braceAngle )*68;
    var braceDy    = Math.sin( braceAngle )*68;
    var braceDDx   = Math.cos( braceAngle )*3.5;
    var braceDDy   = Math.sin( braceAngle )*3.5;

    var p5 = new DjsPoint( ( 48 - braceDx + braceDDy ) / 2, ( p4.y + p3.y + 1.75)/2 + braceDy/2 + 1.75 );
    var p6 = new DjsPoint( ( 48 - braceDx + braceDDy ) / 2 + 48, ( p4.y + p3.y + 1.75)/2 - braceDy/2 );

    return new DjsDiagram(
        [
            new DjsRectanglePart( p1, p1.plus( 1.75, 96 )),
            new DjsPartLabel( p1.plus( 1.75, 96 ), "Post (1)" ),

            new DjsRectanglePart( p1.plus( 1.75, 0 ), p1.plus( 3.5, 3.5 )),

            new DjsRectanglePart( p2, p2.plus( 1.75, 96 )),
            new DjsPartLabel( p2.plus( 1.75, 96 ), "Post (2)" ),

            new DjsRectanglePart( p2.plus( -1.75, 0 ), p2.plus( 0, 3.5 )),

            new DjsRectanglePart( p22, p22.plus( 1.75, 96 )),
            new DjsPartLabel( p22.plus( 1.75, 96 ), "Post (3)" ),

            new DjsRectanglePart( p22.plus( -1.75, 0 ), p22.plus( 0, 3.5 )),

            new DjsRectanglePart( p3, p3.plus( 96, 3.5 )),
            new DjsPartLabel( p3.plus( 96, 3.5 ), "Plank (1)" ),

            new DjsRectanglePart( p4, p4.plus( 96, 3.5 )),
            new DjsPartLabel( p4.plus( 96, 3.5 ), "Plank (2)" ),

            new DjsClosedPathPart( p5 )
                    .lineTo( p5.plus( braceDx, -braceDy ) )
                    .lineTo( p5.plus( braceDx - braceDDy, -braceDy - braceDDx ))
                    .lineTo( p5.plus( - braceDDy, -braceDDx )),
            new DjsPartLabel( p5.plus( braceDx/2 - braceDDy, -braceDy/2 - braceDDx ), "Brace (1)" ),

            new DjsClosedPathPart( p6 )
                    .lineTo( p6.plus( braceDx, braceDy ) )
                    .lineTo( p6.plus( braceDx - braceDDy, braceDy + braceDDx ))
                    .lineTo( p6.plus( - braceDDy, braceDDx )),
            new DjsPartLabel( p6.plus( braceDx/2 - braceDDy, braceDy/2 + braceDDx ), "Brace (2)" )
        ],
        params );
} );


