class FootBrace extends DjsElement {
    constructor( start, angle ) {
        super();
        this.start = start;
        this.angle = angle;
    }

    convertToSvg( params ) {
        var p  = this.start;

        var delegate = new DjsClosedPathPart( p )
                    .lineTo( p.plus( 22, 0 ).rotateByAngle( p, this.angle ))
                    .lineTo( p.plus( 22 - 3.5, 3.5 ).rotateByAngle( p, this.angle ))
                    .lineTo( p.plus( 3.5, 3.5 ).rotateByAngle( p, this.angle ));

        return delegate.convertToSvg( params );
    }
}

djsRender( document.currentScript, function( params ) {
    var p      = new DjsPoint( 0, 0 );

    var braceX = (48-3.5)/2;
    var braceY = 22 * Math.sqrt( 0.5 );
    var pPost = p.plus( braceX, 0 );

    return new DjsDiagram(
        [
            new DjsRectanglePart( p, p.plus( 48, 3.5 )),
            new DjsPartLabel( p.plus( 48, 3.5 ), "Foot" ),

            new DjsRectanglePart( pPost, pPost.plus( 3.5, 96 )),
            new DjsPartLabel( pPost.plus( 3.5, 96 ), "Post" ),

            new FootBrace( new DjsPoint( braceX, braceY ), -135 ),
            new DjsPartLabel( new DjsPoint( braceX - braceY/2, braceY/2 ), "Foot Brace (1)", 120 ),

            new FootBrace( new DjsPoint( braceX + 3.5 + braceY, 0 ), 135 ),
            new DjsPartLabel( new DjsPoint( braceX + braceY/2 + 3.5, braceY/2 ), "Foot Brace (2)" ),
        ],
        params );
} );
