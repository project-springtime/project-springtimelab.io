---
title: "vertical-frame-2"
summary: "The base wooden frame for a double-sized vertical system."
status: complete
---

## Description

{{< page-summary >}}

For the single-size version, see {{% pageref "../vertical-frame-1/" %}}.

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/vertical-frame-2/images/" />}}

## Overview

The same instructions apply as for [vertical-frame-1](../vertical-frame-1/).

## Bill of materials

{{< bom path="/data/parts/vertical-frame-2/bom.json" >}}

## Drawings

Parts:

<script src="cad/parts.drafting.js"
        data-scale="800/50" data-width="1800" data-height="1500"
        data-origin-offset-x="50" data-origin-offset-y="90"
        data-rounding="2" data-unit='"'></script>

Post assembly:

<script src="cad/post.drafting.js"
        data-scale="800/50" data-width="1700" data-height="1800"
        data-origin-offset-x="50" data-origin-offset-y="90"
        data-rounding="2" data-unit='"'></script>

Frame assembly:

<script src="cad/assembly.drafting.js"
        data-scale="800/50" data-width="1700" data-height="1800"
        data-origin-offset-x="50" data-origin-offset-y="90"
        data-rounding="2" data-unit='"'></script>
