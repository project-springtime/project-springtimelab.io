//
// Cover for the hydroponics horizontal pipe holes
// but with a cutoff on the side, for those holes that are close
// to the end caps.
//
// You want to change the parameter: cutoff
//

use <growing-pipe-hole-cover.scad>

$fn = 64;

in  = 25.4; // inch to mm

pipeD    = 3.5   * in;
holeD    = 2.125 * in;
outsideD = 2.375 * in;
eps      = 0.001;

cutoff = 15;

module holecoverWithCutoff() {
    intersection() {
        translate( [ outsideD/2 + eps - cutoff, outsideD/2 + eps, 0 ] ) {
            holecover();
        };
        cube( [ 100, 100, 100 ] ); // larger than anything else
    }
}

holecoverWithCutoff();
