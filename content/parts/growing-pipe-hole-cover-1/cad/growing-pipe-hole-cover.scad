//
// Cover for the hydroponics horizontal pipe holes
//

$fn = 64;

in  = 25.4; // inch to mm

pipeD    = 3.5   * in;
holeD    = 2.125 * in;
outsideD = 2.375 * in;
eps      = 0.001;

module holecover() {
    translate( [ 0, 0, -pipeD/2 + 15 ] )
    intersection() {
        union() {
            difference() {
                translate( [ 0, 0, pipeD/2 - 15 ] ) {
                    cylinder( h=30+eps, r=outsideD/2 );
                };
                rotate( [ 0, 90, 0 ] ) {
                    cylinder( h = 2 * outsideD, r = pipeD/2, center=true );
                }
            };
            translate( [ 0, 0, pipeD/2 - 15 ] ) {
                cylinder( h=15, r=holeD/2 );
            }
        };
        rotate( [ 0, 90, 0 ] ) {
            cylinder( h = 2 * outsideD, r = pipeD/2 + 5, center=true );
        }
    }
}
holecover();
