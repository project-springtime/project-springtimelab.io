---
title: "growing-pipe-hole-cover-1"
summary: "Covers currently unused holes in a horizontal growing pipe."
status: complete
---

## Description

{{< page-summary >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/growing-pipe-hole-cover-1/images/" />}}

## Overview

This is an entirely optional part, and you can solve the problem differently with some
kind of flap. This is a 3D-printed part that covers unused holes in a horizontal growing
pipe and thus prevents algae from growing.

We provide two models:

* The normal hole cover
* The same hole cover, but with straight trim on one side, as if cut off with a saw.
  This is useful if for some reason, your to-be-covered hole is too close to the growing
  pipe's end cap.

{{< boilerplate-3d-variations "content/parts/growing-pipe-hole-cover-1/cad" >}}

  * [growing-pipe-hole-cover.scad](/parts/growing-pipe-hole-cover-1/cad/growing-pipe-hole-cover.scad)
    is the default, and
  * [growing-pipe-hole-cover-with-cutoff.scad](/parts/growing-pipe-hole-cover-1/cad/growing-pipe-hole-cover-with-cutoff.scad)
    is the version that is cut off. You may need to edit this file to change the location of
    the cutoff to match your requirements

{{< /boilerplate-3d-variations >}}
