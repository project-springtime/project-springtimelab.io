---
title: "reservoir-pump-assembly-2"
summary: "Assembly of reservoir, 12V pump and rising pipe"
status: started
---

## Description

{{< page-summary >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/reservoir-pump-assembly-2/images/" />}}
