---
title: "vertical-frame-1"
summary: "The base wooden frame for a vertical system."
status: complete
---

## Description

{{< page-summary >}}

For a double-size version, see {{% pageref "../vertical-frame-2/" %}}.

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/vertical-frame-1/images/" />}}

Note that one of the photos shows attached arms, which are out of scope for this
part.

## Overview

This vertical frame is sturdy, flexible, and fairly simple to build. We use
standard, low-cost 2"x4"x8' lumber, and some wood screws. No great
precision is required either.

You need these tools:

 * a drill for the screw holes
 * a drill bit to drill "through" holes for the screws (i.e. holes that the
   screw slides into)
 * a (smaller) drill bit to pre-drill the holes into which the screw will
   screw into
 * a saw to cut the 2x4s. Almost any saw will do, as long as you bring
   muscle power and/or patience. If you have a miter saw, or other power saw, use that.
 * measuring tape

It's good to also have these tools:

 * large square
 * clamps that can open to 6 inches
 * a helping person or two, for assembly.

## Bill of materials

{{< bom path="/data/parts/vertical-frame-1/bom.json" >}}

## Drawings

Parts:

<script src="cad/parts.drafting.js"
        data-scale="800/50" data-width="1700" data-height="1500"
        data-origin-offset-x="50" data-origin-offset-y="90"
        data-rounding="2" data-unit='"'></script>

Post assembly:

<script src="cad/post.drafting.js"
        data-scale="800/50" data-width="1700" data-height="1800"
        data-origin-offset-x="50" data-origin-offset-y="90"
        data-rounding="2" data-unit='"'></script>

Frame assembly:

<script src="cad/assembly.drafting.js"
        data-scale="800/50" data-width="1700" data-height="1800"
        data-origin-offset-x="50" data-origin-offset-y="90"
        data-rounding="2" data-unit='"'></script>

## Construction

First we cut the parts:

1. Cut two of the 2x4s in the middle, so you have 4 2x4s, about 4ft long
   each. Two will form the feet, and two will become the horizontal planks.

1. Two 2x4s will be the posts, and we keep them at their original length.

1. Shorten another of the 2x4s to form the diagonal center brace.

1. Cut another of the 2x4s into the four foot braces per drawing above.

1. Drill the through-holes in the places indicated in the drawings.

Now we create the feet for the posts. Both posts are identical, so do this
twice:

1. Loosely attach the foot to the post.

1. Attach the two foot braces to the foot and the post. Make sure they are at the right
   angle so the post will stand up straight.

1. Tighten all screws.

Now we assemble the structure:

1. Place the two posts upright, 4ft apart from each other. It is easier if you have two
   helping people.

1. Loosely attach the two planks.

1. Measure the angles well, so that the entire structure stands up straight. It may be
   helpful to measure the diagonals and make sure they have the same length.

1. Attach the diagonal brace.

