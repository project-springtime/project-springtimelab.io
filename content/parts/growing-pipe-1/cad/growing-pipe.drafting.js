djsRender( document.currentScript, function( params ) {
    var p1  = new DjsPoint( 0, 20 );
    var p2  = new DjsPoint( 0,  4 );

    var l   = 5 * 12;
    var w   = 3.5;
    var r   = 2.125/2;
    var dx1 = (5 * 12 -  6 * 8 ) / 2;
    var dx2 = (5 * 12 - 16 * 3  ) / 2;

    var elements = [
        new DjsRectanglePart( p1, p1.plus( l, w )),
        new DjsPartLabel( p1.plus( 5 * 12, w ), "growing-Pipe-1-a" ),
        new DjsLinearMeasure( p1, p1.plus( l, 0 ), 1, 100 ),
        new DjsLinearMeasure( p1.plus( l, 0 ), p1.plus( l, w ) ),

        new DjsLinearMeasure( p1.plus( 0, w/2 ), p1.plus( dx1, w/2 ), 1, 80 ),
        new DjsRadialMeasure( p1.plus( dx1, w/2 ), r ),

        new DjsRectanglePart( p2, p2.plus( 5 * 12, w )),
        new DjsPartLabel( p2.plus( 5 * 12, w ), "growing-Pipe-1-b" ),
        new DjsLinearMeasure( p2, p2.plus( l, 0 ), 1, 100 ),
        new DjsLinearMeasure( p2.plus( l, 0 ), p2.plus( l, w ) ),

        new DjsLinearMeasure( p2.plus( 0, w/2 ), p2.plus( dx2, w/2 ), 1, 80 ),
    ];

    for( var i=0 ; i<7 ; i+=1 ) {
        elements.push( new DjsCirclePart( p1.plus( dx1 + 8 * i, w/2 ), r ));
        if( i>0 ) {
            elements.push( new DjsLinearMeasure( p1.plus( dx1 + 8 * (i-1), w/2 ), p1.plus( dx1 + 8 * i, w/2 ), 1, 80 ));
        }
    }

    for( var i=0 ; i<17 ; i+=1 ) {
        elements.push( new DjsCirclePart( p2.plus( dx2 + 3 * i, w/2 ), r ));
        if( i>0 ) {
            elements.push( new DjsLinearMeasure( p2.plus( dx2 + 3 * (i-1), w/2 ), p2.plus( dx2 + 3 * i, w/2 ), 1, 80 ));
        }
    }
    return new DjsDiagram( elements, params );
} );
