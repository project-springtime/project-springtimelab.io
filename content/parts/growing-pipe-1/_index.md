---
title: "growing-pipe-1"
summary: "Horizontal pipe with holes for net pots in which the plants grow."
status: complete
---

## Description

{{< page-summary >}}

## Images

{{< gallery dir="/parts/growing-pipe-1/images/" />}}

## Drawings

(top view)

<script src="cad/growing-pipe.drafting.js"
        data-scale="800/35" data-width="1700" data-height="800"
        data-origin-offset-x="50" data-origin-offset-y="90"
        data-rounding="2" data-unit='"'></script>

## Material

3"x10' PVC pipe schedule 40. One 10' pipe is cut in half to makes two growing pipes.

Schedule 20 pipe may be cheaper and easier to work with. However, it is hard to find.
Note that all the measurements on this site assume schedule 40.

## Tools

* Reciprocating saw to cut the pipe in half
* Drill
* Hole saw bit 2 1/8"
* Bit suitable for pre-drilling (e.g. 1/4")
* Deburrer (optional; some sand paper and patience will do as well)

## Construction

There are two variations: (1) for mature plants, with a distance of 8" between
plants, and (2) for young plants, with only 3" between plants.

1. Cut the pipe in half.

1. Mark the centers of the growing holes in one straight line. This works well if you
   clamp a steal measuring tape to the pipe (as shown in the photo) and then move along the
   tape.

1. Pre-drill the center holes with the small bit

1. Drill the growing holes. Take your time and try not to get the hole cutter wedged.
   Some people say reversing the direction of your drill before getting all the way
   through produces smoother edges.

