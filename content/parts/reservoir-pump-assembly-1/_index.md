---
title: "reservoir-pump-assembly-1"
summary: "Assembly of reservoir, 120V pump and rising pipe"
status: started
---

## Description

{{< page-summary >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/reservoir-pump-assembly-1/images/" />}}
