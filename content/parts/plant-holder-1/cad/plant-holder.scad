$fn = 64;

intersection() {
    union() {
        difference() {
            union() {
                cylinder( h = 3, r = 30 );
                cylinder( h = 6, r = 25 );
            }
            translate( [ 0, 0, -1 ] ) {
                cylinder( h = 8, r = 9 );
            }
        };

        // make the cutout smooth, so plants won't get hurt
        translate( [ 0, 0, 3 ] )
        rotate_extrude()
        translate([9, 0, 0]) {
            circle(r = 3);
        }
    };
    translate( [ -50, 0, -1 ] ) {
        cube( [ 100, 100, 100 ] );
    }
}

