---
title: "plant-holder-3"
summary: "Helps taller and heavier plants stand up and not topple over."
status: complete
---

## Description

{{< page-summary >}}

See also {{% pageref "../plant-holder-1/" %}} and {{% pageref "../plant-holder-2/" %}}.

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/plant-holder-3/images/" />}}

## Overview

This entirely optional 3D-printed part (which comes in three pieces) is used to help
taller, and top-heavy plants to not topple over. This may happen because the growing
cubes are not particularly stable in the net pots, and the plant may not have managed
to hold on to the net pot very well.

The top piece leaves out a 90 degree gap. Most plants only lead to one side, so
turn the piece accordingly.

{{< boilerplate-3d-many "content/parts/plant-holder-3/cad" >}}

  * [plant-holder-bottom.scad](/parts/plant-holder-3/cad/plant-holder-bottom.scad)
  * [plant-holder-super.scad](/parts/plant-holder-3/cad/plant-holder-super.scad)

{{< /boilerplate-3d-many >}}
