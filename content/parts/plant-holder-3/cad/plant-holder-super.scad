$fn = 64;
play = 0.15;

difference() {
    union() {
        difference() {
            union() {
                cylinder( h = 6, r = 20 );
            }
            translate( [ 0, 0, -1 ] ) {
                cylinder( h = 8, r = 9 );
            }
        }

        // make the cutout smooth, so plants won't get hurt
        translate( [ 0, 0, 3 ] )
        rotate_extrude()
        translate([9, 0, 0]) {
            circle(r = 3);
        }

        for( x=[-12:24:12] ) {
            for( y=[-12:24:12] ) {
                translate( [ x, y, 0 ] ) {
                    cylinder( h=30, r=3.5 );
                    translate( [ 0, 0, 29 ] ) {
                        cylinder( h=6, r = 3 - play );
                    }
                }
            }
        }
    };

    translate( [ 0, 0, -1 ] ) {
        cube( [ 30, 30, 37 ] );
    }
}
