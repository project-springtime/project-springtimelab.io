djsRender( document.currentScript, function( params ) {
    var p = new DjsPoint( 2, 5 );
    var w1 = 1.875;
    var w2 = 1.75;
    var w3 = 3.75 / 2;

    return new DjsDiagram(
        [
            new DjsRectanglePart( p.plus( 0, -0.625 ), p.plus( 11, 0 )),
            new DjsRectanglePart( p.plus( 11/2 - w1/2 - w2, 0 ), p.plus( 11/2 - w1/2,      w3 )),
            new DjsRectanglePart( p.plus( 11/2 + w1/2     , 0 ), p.plus( 11/2 + w1/2 + w2, w3 )),

            new DjsPartLabel( p.plus( 5.5, -0.625 ), "germination-tray-holder-1", -60 )
        ],
        params );
} );
