djsRender( document.currentScript, function( params ) {
    var p = new DjsPoint( 2, 0 );
    var w1 = 1.875;
    var w2 = 1.75;

    return new DjsDiagram(
        [
            new DjsRectanglePart( p, p.plus( 22, 11 )),
            new DjsRectanglePart( p.plus( 0, 11/2 - w1/2 - w2 ), p.plus( 22, 11/2 - w1/2 )),
            new DjsRectanglePart( p.plus( 0, 11/2 + w1/2      ), p.plus( 22, 11/2 + w1/2 + w2 )),

            new DjsPartLabel( p.plus( 11, 11 ), "germination-tray-holder-1" )
        ],
        params );
} );
