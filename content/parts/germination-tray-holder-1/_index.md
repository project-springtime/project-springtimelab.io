---
title: "germination-tray-holder-1"
summary: "Shelf for placing a germination tray on a horizontal 2x4."
status: complete
---

## Description

{{< page-summary >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/germination-tray-holder-1/images/" />}}

## Drawings

Bottom view:

<script src="cad/germination-tray-holder-bottom.drafting.js"
        data-scale="800/40" data-width="800" data-height="500"
        data-origin-offset-x="50" data-origin-offset-y="90"
        data-rounding="2" data-unit='"'></script>

Side view:

<script src="cad/germination-tray-holder-side.drafting.js"
        data-scale="800/40" data-width="800" data-height="300"
        data-origin-offset-x="50" data-origin-offset-y="90"
        data-rounding="2" data-unit='"'></script>

## Materials

* 22"x11" plywood sheet
* 2"x4"x22", cut in half lengthwise, or similar. Can be somewhat shorter (as shown in
  the photograph)
* Screws or wood glue
* Wood stain

## Tools

Depending on method of construction.

## Construction

Glue or screw on the two rails approximately in the middle of the plywood sheet.
They need to be parallel, and a tad further apart than the width of a 2"x4", so they
sit on the lower plank of the Vertical System without getting stuck.
