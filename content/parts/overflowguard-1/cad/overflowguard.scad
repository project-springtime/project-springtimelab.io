$fn = 64;
eps = 0.01;

r            = 20;       // overall radius
r2           = 1.2;      // radius of the mesh
r3           = 23/2 + 2; // outside of inner cylinder
r4           = 15/2;     // see into the pipe: inner radius of pipe
r_pipe_outer = 22.5/2;
w            = 6;

union() {
    difference() {
        union() {
            cylinder( h=2 , r=r+r2 ); // bottom plate
            cylinder( h=25, r = r3 ); // to-be-hollowed-out middle
        };
        translate( [ 0, 0, -eps ] ) { // look into the pipe
            cylinder( h = 15 + 2 * eps, r = r4 );
        };
        translate( [ 0, 0, 10 ] ) { // produces saddle on which pipe rests
            cylinder( h = 15 + eps, r = r_pipe_outer );
        };

        for( i=[0:60:360] ) {
            rotate( i, [ 0, 0, 1 ] )
            union() {
                translate( [ -r3-eps, -w/2, 2+w/2 ] ) {
                    cube( [ 2 * ( r3 + eps ), w, 8 ] );
                }
                translate( [ 0, 0, 2+w/2 ] )
                rotate( 90, [ 0, 1, 0 ] ) {
                    cylinder( r=w/2, h = 2 * ( r3 + eps ), center=true );
                }
                translate( [ 0, 0, 5+w/2+5 ] )
                rotate( 90, [ 0, 1, 0 ] ) {
                    cylinder( r=w/2, h = 2 * ( r3 + eps ), center=true );
                }
            }
        }
    }

    // fence
    for( i=[0:20:340] ) {
        linear_extrude(height = 25, convexity = 10, twist = 30)
        translate( [r * cos(i), r * sin(i), 0])
        circle(r2);

        linear_extrude(height = 25, convexity = 10, twist = -30)
        translate( [r * cos(i), r * sin(i), 0])
        circle(r2);
    };

    // top ring
    translate( [ 0, 0, 23 ] ) {
        difference() {
            cylinder( h=2 , r=r+r2 );
            translate( [ 0, 0, -eps ] ) {
                cylinder( h=2+2*eps, r=r-r2 );
            }
        }
    };
}
