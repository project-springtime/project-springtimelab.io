---
title: "overflowguard-1"
summary: "Prevents roots from clogging and overflowing draining cascading pipes."
status: complete
---

## Description

{{< page-summary >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/overflowguard-1/images/" />}}

## Overview

This entirely optional 3D-printed part can be used to keep long roots away from the
inflow of the cascading pipes that drain the water from the growing pipe. This makes
it less likely that the roots clog the system and the water overflows instead of
draining.

{{< boilerplate-3d-many "content/parts/overflowguard-1/cad" >}}

  * [overflowguard.scad](/parts/overflowguard-1/cad/overflowguard.scad)

{{< /boilerplate-3d-many >}}
