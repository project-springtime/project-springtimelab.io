$fn = 64;

in    = 25.4; // inch to mm

pipeD    = 3.5   * in;
holeD    = 54.5;
outsideD = 2.375 * in;
eps      = 0.01;
potD     = 52;

module potholder() {
    difference() {
        union() {
            difference() {
                cylinder( h=18, r=outsideD/2 );

                translate( [ 0, 0, pipeD/2 + 2 ] )
                rotate( [ 0, 90, 0 ] ) {
                    cylinder( h = outsideD+2*eps, r = pipeD/2, center=true );
                }
            };
            cylinder( h=15, r=holeD/2 );
        };
        translate( [ 0, 0, -eps ] ) {
            cylinder( h=18+2*eps, r=potD/2 );
        };
    };
}
potholder();
