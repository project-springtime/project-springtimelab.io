---
title: "pot-holder-1"
summary: "Helps keep the net pot upright."
status: complete
---

## Description

{{< page-summary >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/pot-holder-1/images/" />}}

## Overview

This entirely optional 3D-printed part keeps the net pot from "rolling" on the round
growing pipe. This may otherwise happen if the plant is tall and top-heavy.

{{< boilerplate-3d-many "content/parts/pot-holder-1/cad" >}}

  * [pot-holder.scad](/parts/pot-holder-1/cad/pot-holder.scad)

{{< /boilerplate-3d-many >}}
