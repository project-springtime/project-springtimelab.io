---
title: "arm-1"
summary: "Arm holding up a horizontal growing pipe."
status: complete
---

## Description

{{< page-summary >}}

Compare with {{% pageref "../arm-2/" %}}.

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/arm-1/images/" />}}

## Drawings

<script src="cad/arm.drafting.js"
        data-scale="800/10" data-width="1000" data-height="600"
        data-origin-offset-x="50" data-origin-offset-y="90"
        data-rounding="2" data-unit='"'></script>

## Material

Plywood, stained after cutting. Either as a sheet, or pre-cut into 10"x5" pieces.

## Required tools

* Table saw (if you get the plywood as a big sheet), or clamps for your working table
  (if you get them pre-cut as 10"x5" pieces)
* Drill
* Hole drill bit 3.5"
* Drill bit 1/4"

## Construction

If you have a table saw and cut your plywood sheet yourself, it is easiest to construct two arms
at the same time:

* With the table saw, cut a square sheet (10x10") of plywood.
* Mark the center of the 3.5" hole, and cut out the hole with the drill.
* The photo above shows the workpiece after this step, with a pencil line in
  preparation of the next step.
* With the table saw, cut the square sheet into two identical arms.
* Mark the screw holes and drill them.
* Stain the part.

If you start with the pre-cut 10"x5" pieces:

* Tightly clamp two of the pieces next to each other, touching along the long side.
  Pretend they haven't been separated from each other yet.
* Mark the center of the 3.5" hole, and cut out the hole (i.e. half hole in each
  piece, but cut at the same time)
* Mark the screw holes and drill them.
* Stain the part.
