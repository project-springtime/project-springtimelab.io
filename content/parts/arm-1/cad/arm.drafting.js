djsRender( document.currentScript, function( params ) {
    var p  = new DjsPoint( 0, 0 );
    var dx = 3;
    var r  = 3.5/2;

    var hole1p = p.plus( 10-1, 1 );
    var hole2p = p.plus( 10-3.5+1, 5-1 );

    return new DjsDiagram(
        [
            new DjsClosedPathPart( p )
                    .lineTo( p.plus( 10, 0 ))
                    .lineTo( p.plus( 10, 5 ))
                    .lineTo( p.plus( dx + r, 5 ))
                    .arcTo( p.plus( dx - r, 5 ), r, 1 )
                    .lineTo( p.plus( 0, 5 )),
            new DjsCirclePart( hole1p, 0.25/2 ),
            new DjsCirclePart( hole2p, 0.25/2 ),
            new DjsSymmetryLine( p.plus( dx, 5 - 1.5 * r ), p.plus( dx, 5 + 0.5 * r )),

            new DjsLinearMeasure( hole1p, p.plus( 10, hole1p.y ), 1, 130 ),
            new DjsLinearMeasure( hole2p, p.plus( 10, hole2p.y ), 0, 120 ),

            new DjsLinearMeasure( hole1p, p.plus( hole1p.x, 0 ), 0, 120 ),
            new DjsLinearMeasure( hole2p, p.plus( hole2p.x, 0 ), 0, 270 ),

            new DjsLinearMeasure( p, p.plus( 10, 0 ), 1, 80 ),
            new DjsLinearMeasure( p.plus( 10, 0 ), p.plus( 10, 5 ), 1, 110 ),
            new DjsLinearMeasure( p.plus( 10, 5 ), p.plus( dx, 5 ), 1, 70 ),

            new DjsRadialMeasure( p.plus( dx, 5 ), r, -135 ),

            new DjsPartLabel( p.plus( 1, 5 ), "Arm-1" )
        ],
        params );
} );
