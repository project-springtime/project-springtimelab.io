---
title: "reservoir-inflow-pipe-1"
summary: "Pipe that drains the lowest growing pipe into the reservoir"
status: images missing
---

## Description

{{< page-summary >}}

## Bill of materials

{{< bom path="/data/parts/reservoir-inflow-pipe-1/bom.json" >}}

## Drawings

No distances are given as they depend on the actual distances in your assembled system,
and assuming you got everything at perfectly the right distances seems like a bad idea here.
Note that most of these pipes will likely have different dimensions in your system and no
two will be totally alike.

<script src="cad/reservoir-inflow-pipe.drafting.js"
        data-scale="800/15" data-width="1000" data-height="300"
        data-origin-offset-x="50" data-origin-offset-y="90"
        data-rounding="2" data-unit='"'></script>

## Required tools

* PVC pipe cutter, or a handsaw and file, to cut the PVC pipe to the correct lengths
  and deburr

## Construction

This is a little tricky, because this pipe needs to be of the correct length and
that correct length is likely to be not what you think due to:

* tolerances of the growing pipes and their holes, compared to the blueprints;
* the inherit imprecision how deeply the pipe inserted into the elbow will actually
  go before the glue starts making adjustments impossible.

It is recommended to leave the pipe ends longer than necessary until final assembly of
the system, when they can be cut if needed.
