---
title: "germination-fence-1"
summary: "Visual separator for germinating seeds."
status: obsolete
---

## Description

**This part is obsolete.** The cube holder [cubeholder-1](../cubeholder-1/) with label
solves the problem better.

{{< page-summary >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/germination-fence-1/images/" />}}

## Overview

This is an entirely optional part. We just noticed that when germinating different
types of seeds, perhaps starting at different times, in the same germination tray, it
can be difficult to keep them separate and remember which is which.

So we created this germination fence, which just sits in the tray as a visual separator
between the different batches of germinating seeds.

{{< boilerplate-3d-many "content/parts/germination-fence-1/cad" >}}

  * [germination-fence.scad](/parts/germination-fence-1/cad/germination-fence.scad)

{{< /boilerplate-3d-many >}}
