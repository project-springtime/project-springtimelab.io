$fn = 32;

inch   = 25.4;
length = 9.5 * inch;
height = 30;
width  = 20;
wall   = 2;

windowwidth = 10;
windowheight = 25;

step   = ( length - 3*windowwidth ) / 16;

difference() {
    union() {
        // bottom plate
        translate( [ 0, -width/2, 0 ] ) {
            cube( [ length, width, wall ] );
        }

        // chamfer
        translate( [ 0, wall/2, wall ] ) {
            difference() {
                cube( [ length, wall, wall ] );
                translate( [ length, wall, wall ] )
                rotate( [ 0, 90, 0 ] ) {
                    cylinder( r=wall, h=length*2, center=true );
                }
            }
        }

        // chamfer
        translate( [ 0, -3*wall/2, wall ] ) {
            difference() {
                cube( [ length, wall, wall ] );
                translate( [ length, 0, wall ] )
                rotate( [ 0, 90, 0 ] ) {
                    cylinder( r=wall, h=length*2, center=true );
                }
            }
        }

        // vertical wall
        translate( [ 0, -wall/2, 0 ] ) {
            cube( [ length, wall, height ] );
        }
    };

    // windows
    for( i=[ windowwidth : step : length-windowwidth ] ) {
        translate( [ i, 2*wall, wall ] )
        rotate( [ 90, 0, 0 ] )
        linear_extrude( wall * 3 ) {
            polygon( [
                  [ 0, 0 ],
                  [ windowwidth, 0 ],
                  [ windowwidth, windowheight - windowwidth/2 ],
                  [ windowwidth/2, windowheight ],
                  [ 0, windowheight - windowwidth/2 ]
            ] );
        }
    }
};
