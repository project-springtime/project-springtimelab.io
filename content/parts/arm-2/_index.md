---
title: "arm-2"
summary: "Arm holding up two horizontal growing pipes."
status: in progress
---

## Description

{{< page-summary >}}

Compare with {{% pageref "../arm-1/" %}}.

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/arm-2/images/" />}}

## Drawings

<script src="cad/arm.drafting.js"
        data-scale="800/10" data-width="1500" data-height="650"
        data-origin-offset-x="50" data-origin-offset-y="90"
        data-rounding="2" data-unit='"'></script>

## Material

Plywood, stained after cutting. Either as a sheet, or pre-cut into 15"x5" pieces.

## Required tools

* Table saw (if you get the plywood as a big sheet), or clamps for your working table
  (if you get them pre-cut as 10"x5" pieces)
* Drill
* Hole drill bit 3.5"
* Drill bit 1/4"

## Construction

If you have a table saw and cut your plywood sheet yourself, it is easiest to construct two arms
at the same time:

* With the table saw, cut a square sheet (15x10") of plywood.
* Mark the center of the 3.5" holes, and cut out the holes with the drill.
* With the table saw, cut the square sheet into two identical arms.
* Mark the screw holes and drill them.
* Stain the part.

If you start with the pre-cut 15"x5" pieces:

* Tightly clamp two of the pieces next to each other, touching along the long side.
  Pretend they haven't been separated from each other yet.
* Mark the center of the 3.5" holes, and cut out the holes (i.e. half hole in each
  piece, but cut at the same time)
* Mark the screw holes and drill them.
* Stain the part.
