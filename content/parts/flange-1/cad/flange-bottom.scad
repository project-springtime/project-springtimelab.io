include <constants.inc>

h = 2;   // distance from bottom to lowest point of cutout

difference() {
    union() {
        difference() {
            // main body
            cylinder( r = R, h = h + growingOutsideR/2 ); // far too high

            // shape of the growing pipe
            translate( [ 0, 0, growingOutsideR + h ] )
            rotate( [ 0, 90, 0 ] ) {
                cylinder( r = growingOutsideR, h = 2*R+2*eps, center = true );
            }
        };

        // inner wall -- bottom
        translate( [ 0, 0, h - eps ] ) {
            cylinder( r = pipeR+4, h = growingT + eps );
        };

        // slanted inner wall -- top
        translate( [ 0, 0, h + growingT - eps ] ) {
            cylinder( r1 = pipeR+4, r2 = pipeR+3, h = 16 + eps );
        };
    };

    // hole for the pipe
    translate( [ 0, 0, -eps ] ) {
        cylinder( r = pipeR, h = h + growingOutsideR/2 + 2 * eps );
    }
}
