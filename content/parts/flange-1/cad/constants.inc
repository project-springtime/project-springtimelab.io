
// general constants

in = 25.4;
$fn = 64;
eps = 0.01;


R     = 54 / 2 - 2;          // radius of the main cylinder -- needs to fit through growing hole
pipeR = 21.5 / 2 + 0.2;      // radius of the hole through which the pipe will fit

growingT        = 6;                          // wall thickness of the growing pipe
growingOutsideR = 90 / 2;                     // outside radius of the growing pipe
growingInsideR  = growingOutsideR - growingT; // inside radius of the growing pipe

