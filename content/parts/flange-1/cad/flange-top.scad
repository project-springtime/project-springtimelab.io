include <constants.inc>

h  = 12;   // distance from top to bottom of this part

difference() {
    union() {
        intersection() {
            // main body
            cylinder( r = R, h = h );

            // shape of the growing pipe
            translate( [ 0, 0, growingInsideR ] )
            rotate( [ 0, 90, 0 ] ) {
                cylinder( r = growingInsideR, h = 2*R+2*eps, center = true );
            }
        };
    };

    // hole for the pipe
    translate( [ 0, 0, -eps ] ) {
        cylinder( r = pipeR, h = h + growingOutsideR/2 + 2 * eps );
    }

    // cutout for the lower part to fit in
    translate( [ 0, 0, -eps ] ) {
        cylinder( r1 = pipeR+4, r2 = pipeR+3, h = 16 + 2 * eps );
    }
}
