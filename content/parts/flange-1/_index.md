---
title: "flange-1"
summary: "For better seal of the cascading pipe connecting to the growing pipe"
status: complete
---

## Description

{{< page-summary >}}

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/flange-1/images/" />}}

## Overview

This is an optional part. It has two parts: a top and a bottom, which need
to be 3D-printed. Once printed, they slide into each other from inside
and outside the drainage hole of the growing pipe, holding the exiting
cascading pipe in place.

As 3D printing does not produce smooth surfaces, using this part requires
suitable caulking (outdoors, waterproof, preferably flexible).

{{< boilerplate-3d-many "content/parts/flange-1/cad" >}}

  * [flange-top.scad](/parts/flange-1/cad/flange-top.scad)
  * [flange-bottom.scad](/parts/flange-1/cad/flange-bottom.scad)
  * [constants.inc](/parts/flange-1/cad/constants.inc)

{{< /boilerplate-3d-many >}}
