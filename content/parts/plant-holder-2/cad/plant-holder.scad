$fn = 64;
eps = 0.01;

intersection() {
    union() {
        difference() {
            union() {
                intersection() {
                    cylinder( h = 3, r = 30 );
                    translate( [ -30-eps, -20, -eps ] ) {
                        cube( [ 60+2*eps, 40, 3+2*eps ] );
                    }
                };
                cylinder( h = 6, r = 24 );
            }
            translate( [ 0, 0, -1 ] ) {
                cylinder( h = 8, r = 9 );
            }
        };

        // make the cutout smooth, so plants won't get hurt
        translate( [ 0, 0, 3 ] )
        rotate_extrude()
        translate([9, 0, 0]) {
            circle(r = 3);
        }
    };
    translate( [ -50, 0, -1 ] ) {
        cube( [ 100, 100, 100 ] );
    }
}

