---
title: "plant-holder-2"
summary: "Helps taller and heavier plants stand up and not topple over."
status: complete
---

## Description

{{< page-summary >}}

See also {{% pageref "../plant-holder-1/" %}} and {{% pageref "../plant-holder-3/" %}}.

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/plant-holder-2/images/" />}}

## Overview

This entirely optional 3D-printed part (which comes in two pieces) is used to help
taller, and top-heavy plants to not topple over. This may happen because the growing
cubes are not particularly stable in the net pots, and the plant may not have managed
to hold on to the net pot very well.

Unlike [plant-holder-1](../plant-holder-1/), this version cuts off a piece of
the cover, which makes it possible to label net pots and still see the label even
after the holder has been put on top.

{{< boilerplate-3d-many "content/parts/plant-holder-2/cad" >}}

  * [plant-holder.scad](/parts/plant-holder-2/cad/plant-holder.scad)

{{< /boilerplate-3d-many >}}
