//
// Parameterizable module for creating a variety of growing cube holders
//

inch    = 25.4;
dW      = 1.2 * inch;
dD      = 1.2 * inch;
width1  = 10;
height1 = 6;
height2 = 30;
t       = 2;

module CubeHolderWall( dW ) {
    translate( [ 0, t, 0 ] )
    rotate( [ 90, 0, 0 ] )
    linear_extrude( t ) {
        polygon( [
              [ 0,      0 ],
              [ dW + t, 0 ],
              [ dW + t, height2 ],
              [ dW,     height2 ],
              [ dW + t - width1, height1 ],
              [ width1, height1 ],
              [ t,      height2 ],
              [ 0,      height2 ]
        ] );
    }
}

module CubeHolder( nX, nY, dX = 1, dY = 1 ) {
    for( y = [0:dY:nY-1] ) {
        for( x = [0:dX:nX-1] ) {
            translate( [ x * dW, y * dD, 0 ] ) {

               union() {
                    CubeHolderWall( dX * dW );

                    translate( [ t, 0, 0 ] )
                    rotate( [ 0, 0, 90 ] ) {
                        CubeHolderWall( dY * dD );
                    }

                    translate( [ 0, dY * dD, 0 ] ) {
                        CubeHolderWall( dX * dW );
                    }
                    translate( [ dX * dW + t, 0, 0 ] )
                    rotate( [ 0, 0, 90 ] ) {
                        CubeHolderWall( dY * dD );
                    }
                }
            }
        }
    }
}
