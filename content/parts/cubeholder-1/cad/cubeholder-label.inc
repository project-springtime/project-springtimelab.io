//
// Adds a protrusion to the cube holder module that makes it easy to
// label the cube holder with a marker or sticker
//

labelW = 12;
labelL = labelW;

include <cubeholder.inc>

module CubeHolderLabel( nX, nY, dX = 1, dY = 1 ) {
    union() {
        CubeHolder( nX, nY, dX, dY );

        translate( [ labelL, t, 0 ] )
        rotate( [ 90, 0, -90 ] )
        linear_extrude( labelL ) {
            polygon( [
                [ 0, 0 ],
                [ 0, height2 ],
                [ labelW, height2 ]
            ] );
        }
    }
}
