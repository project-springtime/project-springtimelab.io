---
title: "cubeholder-1"
summary: "Keeps small sections of the growing cube material from topping over in the germination tray."
status: complete
---

## Description

{{< page-summary >}}

There are several variations.

## Images

{{< load-photoswipe >}}
{{< gallery dir="/parts/cubeholder-1/images/" />}}

## Overview

This is an entirely optional part. It is convenient if you germinate small batches of
seeds, and break the cube material into relatively small pieces. These small pieces
tend to topple over in the germination tray, drowning the plant.

This part, in some variations, prevents the growing cube(s) from toppling over.

The most convenient variation seems to be the 3x3 with label: as growing cube sheets
apparently come organized in rows of 9, one such row can be broken off and, in three
3-cube pieces, inserted into the cube holder. Write some identifier (like a letter) on
the label protrusion, and you can easily keep track which seed is which even for
small batches.

{{< boilerplate-3d-variations "content/parts/cubeholder-1/cad" >}}

  * most convenient: [cubeholder-3x3-3x1-label.scad](/parts/cubeholder-1/cad/cubeholder-3x3-3x1-label.scad)
  * [cubeholder-1x3-1x3.scad](/parts/cubeholder-1/cad/cubeholder-1x3-1x3.scad)
  * [cubeholder-2x1.scad](/parts/cubeholder-1/cad/cubeholder-2x1.scad)
  * [cubeholder-2x2-2x1.scad](/parts/cubeholder-1/cad/cubeholder-2x2-2x1.scad)
  * [cubeholder-2x2.scad](/parts/cubeholder-1/cad/cubeholder-2x2.scad)
  * [cubeholder-3x2-3x1.scad](/parts/cubeholder-1/cad/cubeholder-3x2-3x1.scad)
  * [cubeholder-3x2.scad](/parts/cubeholder-1/cad/cubeholder-3x2.scad)
  * [cubeholder-3x3-3x1.scad](/parts/cubeholder-1/cad/cubeholder-3x3-3x1.scad)
  * [cubeholder-4x2-4x1.scad](/parts/cubeholder-1/cad/cubeholder-4x2-4x1.scad)
  * you also need library files [cubeholder.inc](/parts/cubeholder-1/cad/cubeholder.inc)
    and [cubeholder-label.inc](/parts/cubeholder-1/cad/cubeholder-label.inc).

{{< /boilerplate-3d-variations >}}

