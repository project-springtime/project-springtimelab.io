---
title: About
comments: false
---

## History

This project was inspired when [Johannes Ernst](https://upon2020.com/)
visited [Bruce Gee](http://waterworks-hydro.com/about.html)'s booth
at [Makerfaire Bay Area 2019](https://makerfaire.com/bay-area/) where
Bruce showed off some of his hydroponics projects. Johannes
was suitably impressed and then discovered Bruce's website at
[waterworks-hydro.com](http://waterworks-hydro.com/). On his website,
Bruce not only publicly documented what he showed at Maker Faire, but
several other hydroponics systems he created before, encouraging
everybody to try hydroponics in their backyards, too.

Johannes had already decided that he wanted to learn more about ways of local,
decentralized, non-industrialized food production. Backyard hydroponics
seemed like a great thing to do try, and because Bruce had documented his
setup so well, setting up a similar system seemed quite straightford.
Besides, Johannes likes lettuce, which is the easiest crop to grow
hydroponically! So he went and (mostly) copied Bruce's vertical system.
Here is his initial
[show-and-tell blog post](https://upon2020.com/blog/2019/07/my-diy-backyard-hydroponics-system/).

But Johannes made a few changes and wanted to document those, too, for
the benefit of  the next person building a similar system. Hey, that
person would have an even easier time setting up their system, because they
would be able to benefit from the experience of two people who had built
earlier versions!

Now where have we heard that kind of thing before? Oh, yes. In software:
in [open-source development](https://en.wikipedia.org/wiki/Open-source_model).
I write some code until it does what I want it to do, and publish it. Then
you can come along, download, try it out, and if you feel like it and improve
it, until it additionally does what you want it to do. Voila! Now both
of us got twice the features!  If lots of people contribute, we gain lots
of value while needing only to do a small part of the total work. Sounds like
a great model.

## About

Project Springtime is an attempt to bring this model of collaboration to
decentralized, local food production:

* We document the system we built, how we built it, and how we operate it
  on a day-to-day basis. It's all on this website, which is
  published straight from a
  [Gitlab repository](https://gitlab.com/project-springtime/project-springtime.gitlab.io),
  just like many open-source (software) projects do.

* Over time as we (and others) learn what works and what does not and
  how to do even better, we log bugs, and request new features. We use
  [Gitlab's issue tracking system](https://gitlab.com/project-springtime/project-springtime.gitlab.io/issues),
  also just like in software.

* We encourage you to build this system, too, either as-is or with
  whatever modifications you think would improve it. We would love to
  receive your merge requests (aka pull requests) if you think your
  changes are applicable to others. Of course, we also would love to
  receive your documentation improvements.

Will it work? It worked once, so far, between Bruce and Johannes, even
without [git](https://en.wikipedia.org/wiki/Git).  Beyond that, we have
no idea. But let's try! Everybody benefits if it becomes much simpler to set
up some local food production in our back yards, without having to do much
research or becoming an expert in hydroponics or wood working -- because other
people's experience is effectively reused.

## Note about license

As this is not a software project, it appears Creative Commons licenses
are more appropriate than "code" licenses, so that's what we chose.
It's intended for you and me, who build this kind of thing in our
backyards. Should you intend to commercialize it, we'd ask you to
contribute back.
