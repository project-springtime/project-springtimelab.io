---
title: Vertical Recirculating Deep Water Culture System (10x5ft)
subtitle:  Overview
summary: Grows lettuce, herbs etc. Space-efficient, double-size, for your backyard.
status: in progress
layout: overview
weight: 20
---

## Preface

Compare with {{% pageref "../vert-rdwc-1/" %}}, which is the same but smaller. Use
the instructions from over there.

## Images

{{< load-photoswipe >}}
{{< gallery dir="/systems/vert-rdwc-2/images/" />}}

## Construction

* [Bill of materials](bom/)

