---
title: Vertical Recirculating Deep Water Culture System
subtitle:  Operations
status: complete
---

We break the operation of the system down into different tasks,
and identify when they should be performed.

## System startup tasks

Perform these tasks when you first start operating the system, or if
you start operating it anew in a new growing season, for example.

### Flushing the system

After you are finished with construction, flush the system. This will wash out debris
from construction, and lets you check for leaks. It is advisable to measure how much
water you enter into the system for flushing (and subtract what you discard after flushing),
so you know how much water is in the system when you calculate how many nutrients to add,
as it will be impossible to remove the remaining water in the Growing Pipes after flushing.

1. Fill the reservoir with clean water until well above the top of the pump.

2. Connect the pump to power

3. Check that the water starts running in the top-most Growing Pipe. Then, once the
   this Growing Pipe is overflowing (which might take a few minutes), check that all
   other Growing Pipes gradually fill with water, one at a time, and that eventually,
   water flows back into the reservoir. At the same time, make sure that the reservoir
   does not empty.

4. Check all pipes for leaks and make repairs as necessary.

5. Check that the water levels in all Growing Pipes are such that when you insert a
   net pot into one of the holes, the bottom of the net pot is just a bit under water.

6. Run the system for a few hours, and check for leaks once more.

7. Disconnect the pump from power, and carefully remove the Reservoir from the system.
   This may involve losening the Rising Pipe Assembly's Conduit Strap. You should be
   able to raise the entire Rising Pipe Assembly including the pump at the bottom, so
   you don't need to disconnect the pump.

8. Discard the water in the reservoir and remove any debris that has accumulated in the
   Reservoir.

9. Put the Reservoir back in place, lower the Vertical Pipe Assembly back down and
   re-attach with the Conduit Strap.

{{< box note >}}
**Note:** it is impractical to completely remove the flushing water from the pipes
after you are done flushing. So measure the amount of water you put into
the reservoir, and when you are done flushing, measure and subtract the
amount of dump from the reservoir. Add the remaining amount of water
into your calculation when you create the first batch of nutrient solution.
{{< /box >}}

We estimate the amount of water in the pipes as follows:

* 5 ft pipe length, times 8 pipes: 40 ft = approx 12 m
* inside pipe diameter: 2 7/8 " = approx. 7.3 cm
* the pipes are half full
* 12 m times 3.65 cm times 3.54 cm times Pi times 0.5 = approx. 25 liters = approx. 6.65 gal

### Create the first batch of nutrient solution.

Follow the instructions of the product you are using. For two-part or three-part products,
you can stop after you have created the two or three separate solutions in separate
containers. When it is time to add nutrients to the system, simply mix them into the
water in the reservoir.

## System shutdown tasks

Perform these tasks when you want to shut down the system for some time,
such as for winter, but want to get back to operating it at some point.

tbd.

## Daily tasks

### Check germination water level and replenish as needed

The water level should be sufficient to keep the seed moist, but not drowning.

### Check that water is circulating

Check the outflow pipe that empties into the reservoir. If there is no flow, or only
a trickle, check whether the pump is working, and whether perhaps some pipe is clogged.

### Check reservoir water level and replenish as needed

The pump should be well submerged, but the outflow pipe that empties into the reservoir
should also be well above the water level.

### Check pH and add pH modifiers as needed

Check the pH level in one of your growing pipes, not the reservoir: the reservoir may not
be completely mixed if you just added nutrients or pH modifiers. If you measure where
the roots of your plants are, you measure what they experience.

Note that it may take some time (perhaps hours) until any changes you make in the
reservoir make it into the growing pipes. So wait for some hours after any addition of
nutrients or pH modifiers before you measure again.

The best pH level depends on the plants you are growing. For lettuce, it is about 6.
An acceptable range is probably between 5.5 and 6.5.

### Check TDS and add nutrients as needed

Note all comments made above on testing pH levels and adding pH modifiers. They apply
here as well.

The best TDS level depends on the plants you are growing. For lettuce, it is about 1000.
An acceptable range is probably between 800 and 1200.

### Check for pests and perform remediation as needed

Remove diseased leaves (or plants!). Check for aphids, and mildew and apply appropriate
remedies.

## Weekly tasks

### Transfer sufficiently-grown seedlings

Your seedlings are ready to be moved from the germination try to the growing system when
their roots can reach the water in the growing pipes, and when the plant is tall enough
that there is no danger the water in the growing pipe will drown the plant.

### Start germination of new seeds

Break off a suitable section of your growth medium, and add one seed per cube. (For some
plants you may be able to grow two seeds in the same cube; try one first.)

Place your growing cubes in the germination tray, and ensure that the growing cube is moist
up to the seed, but without drowning it.

## Monthly tasks

### Check seed supplies and reorder as needed

How much do you want to grow next month? You figure it out.

### Check growing supplies and reorder as needed

Getting low? Reorder:

* pH modifiers
* nutrients

## When available

### Harvest

Your plant is ready for harvest when you become too impatient, basically :-)
