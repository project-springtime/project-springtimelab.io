---
title: Vertical Recirculating Deep Water Culture System
subtitle:  Systems model
status: complete
---

The following systems model might be useful. It (attempts to) capture everything there
is to know about how the system operates. In particular, it identifies all inputs to
the operation of the system, all (intended and unintended) outputs of the system, as
well as key environmental factors that catalyze or inhibit the successful operation.

Finally, it identifies the activities necessary to successfully operate the system, and
when they need to be performed. Details are provided when clicking on the hyperlinks.


<div class="process-model">
 <div class="environment">
  <div class="box">
   <h4>Environment/catalysts/inhibitors</h4>
   <ul>
    <li>open air</li>
    <li>sunshine hours and intensity</li>
    <li>temperature and weather</li>
   </ul>
  </div>
 </div>
 <div class="inout">
  <div class="box">
   <h4>Inputs</h4>
   <ul>
    <li>seeds</li>
    <li>horticubes</li>
    <li>nutrients</li>
    <li>water</li>
    <li>electricity</li>
   </ul>
  </div>
  <div>
   <svg class="process-model" viewBox="0 0 1000 600">
    <rect x="200" y="1" width="200" height="599"/>
    <text x="300" y="300" text-anchor="middle">Germination</text>
    <rect x="600" y="1" width="200" height="599"/>
    <text x="700" y="300" text-anchor="middle">Growing</text>
    <path d="M  10 200 l 100 0 l 0 -100 l 80 200 l -80 200 l 0 -100 l -100 0 Z"/>
    <text x="100" y="300" text-anchor="middle">Inputs</text>
    <path d="M 410 200 l 100 0 l 0 -100 l 80 200 l -80 200 l 0 -100 l -100 0 Z"/>
    <text x="500" y="300" text-anchor="middle">Transfer</text>
    <path d="M 810 200 l 100 0 l 0 -100 l 80 200 l -80 200 l 0 -100 l -100 0 Z"/>
    <text x="900" y="300" text-anchor="middle">Outputs</text>
   </svg>
  </div>
  <div class="box">
   <h4>Outputs</h4>
   <ul>
    <li>produce</li>
    <li>horticube waste</li>
   </ul>
  </div>
 </div>
 <div class="operations">
  <div class="box">
   <h4>Operations germination</h4>
   <p>Daily:</p>
   <ul>
    <li><a href="../operations/#check-germination-water-level-and-replenish-as-needed">check germination water level and replenish as needed</a></li>
   </ul>
   <p>Weekly:</p>
   <ul>
    <li><a href="../operations/#transfer-sufficiently-grown-seedlings">transfer sufficiently-grown seedlings</a></li>
    <li><a href="../operations/#start-germination-of-new-seeds">start germination of new seeds</a></li>
   </ul>
   <p>Monthly:</p>
   <ul>
    <li><a href="../operations/#check-seed-supplies-and-reorder-as-needed">check seed supplies and reorder as needed</a></li>
   </ul>
  </div>
  <div class="box">
   <h4>Operations growing</h4>
   <p>Daily:</p>
   <ul>
    <li><a href="../operations/#check-that-water-is-circulating">check that water is circulating</a></li>
    <li><a href="../operations/#check-reservoir-water-level-and-replenish-as-needed">check reservoir water level and replenish as needed</a></li>
    <li><a href="../operations/#check-ph-and-add-ph-modifiers-as-needed">check pH and add pH modifiers as needed</a></li>
    <li><a href="../operations/#check-tds-and-add-nutrients-as-needed">check TDS and add nutrients as needed</a></li>
    <li><a href="../operations/#check-for-pests-and-perform-remediation-as-needed">check for pests and perform remediation as needed</a></li>
   </ul>
   <p>Monthly:</p>
   <ul>
    <li><a href="../operations/#check-growing-supplies-and-reorder-as-needed">check growing supplies and reorder as needed</a></li>
   </ul>
   <p>When available:</p>
   <ul>
    <li><a href="../operations/#harvest">harvest</a></li>
   </ul>
  </div>
 </div>
</div>
