---
title: Vertical Recirculating Deep Water Culture System (5x5ft)
subtitle:  Overview
summary: Grows lettuce, herbs etc. Space-efficient, for your backyard.
status: in progress
layout: overview
weight: 10
---

## Preface

How does one best document a hydroponics system? We don't know! We will
will learn as we try. Let's give it a shot and start with photos, so you can see what
we are talking about.

## Images

{{< load-photoswipe >}}
{{< gallery dir="/systems/vert-rdwc-1/images/" />}}

## Overview

This system is a
[Recirculating Deep Water Culture](https://en.wikipedia.org/wiki/Deep_water_culture)
hydroponics system. "Deep water" means that the plants' roots sit in the water all the time.
"Circulating" means that the water circulates through the system, bringing oxygen and
nutrients to the plants on an ongoing basis. It is intended for your backyard, or some
other place outdoors in the sunshine. It takes:

* about 5 ft times 5 ft of floor space, and is
* between 6 and 9 ft tall, depending on some options (like solar).

As you can see in the photos, the system has a sturdy, central wooden frame, made from common
(and cheap) 2x4's. Its arms are made from (also cheap and sturdy) plywood sheets. They hold up
eight horizontal PVC pipes, four on each side, that hold the growing plants, and through which
the water circulates. The water enters a growing pipe on one side, and spills over into a
thinner PVC pipe on the other side, which leads the water into the next horizontal growing
pipe on the other side, a little bit further down. The spillover level is set so that the
plants' roots can reach the water easily and the plant doesn't drown.

A common storage box with some extra holes serves as a reservoir for the water. The reservoir
also contains a submersed pump, which pumps the water up the vertical pipe from where the
water enters the first horizontal growing pipe through a thin plastic tube. The water ends
up back in the reservoir once it has left the lowest growing pipe. This single path of the
water through the system makes it easy to detect problems, should there be any.

The plants grow inside little net pots placed into holes cut into the horizontal growing
pipes. Six of the growing pipes are for larger plants, so the holes for the net pots are
relatively far apart. Two are for younger and smaller plants that can be placed more closely
together.

(Of course, you could build this differently, based on what your needs are, like with
anything else we describe here. We only document what worked for us. And if something
works better for you, feel free to submit a
[pull request](https://gitlab.com/project-springtime/project-springtime.gitlab.io).)

There are some options, which you may or may not want to install:

* towards the bottom, the system has room for a {{% pageref "/parts/germination-tray-holder-1/" "germination tray" %}}
  in which seeds can be germinated until they have grown enough to be placed in the net pots;
  this works well when it is warm outside, including at nights. When it's not warm enough,
  you need to germinate indoors otherwise your seeds won't sprout.
* at the top, there is room for a solar panel attachment,
  which produces enough energy to power the pump circulating the water.

This system is designed as a collection of modular parts. So we document it one part or
one assembly at a time. This allows us to improve the system one part at a time as well,
without disrupting the rest of the system very much.

## Systems model

* {{% pageref "model/" "System model"%}} -- overview over how the system works: inputs, outputs,
  catalysts etc.

## Construction

Here's all the info you need to build the system:

* {{% pageref "bom/" "Bill of materials" %}}
* {{% pageref "blueprints/" "Blueprints" %}}
* {{% pageref "construction/" "Construction instructions" %}}

## Operation

Have the system built? Time to grow some plants! Here is how:

* {{% pageref "operations/" "Operations" %}}


