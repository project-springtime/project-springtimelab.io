---
title: FAQ
comments: false
---
## What can I grow?

* Lettuce: many more types than you can find in a typical grocery store
* Basil, Cilantro, Parsley, Dill and other herbs
* Kohlrabi
* Celery
* Peppers of various kinds: big sweet ones, and small hot ones
* Tomatoes
* Cucumbers
* Water melons
* Other experiments ongoing

## What have you tried to grow and it didn't work, or not well enough?

* Radishes. The plants grow nicely, but the radish seems confused about where to grow.
  So we ended up with some kind of radish, but it was misshapen and had a hard skin.
  Some plants failed to create a bulb at all.
* Some plants are too large for the system: cauliflower, broccoli, for example

## Can I use this system indoors?

No. This is an outdoor system that relies on the sun, which provides
growing light without the cost of LEDs, installation or electricity!

## The documentation is incomplete, misleading or completely wrong!

You are probably right. That's because writing this up is work in progress,
and we generally publish early and often instead of striving for perfection
before posting it in public.

So: check for updates frequently, and we welcome all help: best is by
sending pull (aka merge) requests to our [repository on Gitlab](https://gitlab.com/project-springtime/).

## I want to help

Check out our [issue tracker](https://gitlab.com/project-springtime/project-springtime.gitlab.io/issues).
Find something you like to work on? Just say so in the comments.

Have an idea? Go to the same
[issue tracker](https://gitlab.com/project-springtime/project-springtime.gitlab.io/issues) and
tell us about it.

