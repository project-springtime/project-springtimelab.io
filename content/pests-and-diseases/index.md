---
title: Pests and Diseases
---

These are grouped by photos showing the same condition.

{{< load-photoswipe >}}

## Dill Problem 1:

{{< gallery dir="/pests-and-diseases/dill-1/images/" />}}

Observation:

* Sections of the stems of the plant are covered in small, greenish insects
  (aphids?)
* This infestation happened very quickly. Before I noticed,
  the plant was already in very bad shape.

Remedy:

* I removed the plant to keep the pests away from other plants.

## Kohlrabi Problem 1:

{{< gallery dir="/pests-and-diseases/kohlrabi-1/images/" />}}

Observation:

* Sections of the branches and the leaves of the plant are covered in small, grayish
  insects of various sizes. When not looking too closely, it looks almost like some
  mortar had been spilled on the plant.

Remedy:

* I removed all affected leaves

## Kohlrabi Problem 2:

{{< gallery dir="/pests-and-diseases/kohlrabi-2/images/" />}}

Observation:

* Large, irregular sections of the leaves are missing
* Little (<1mm) black droppings are accumulating below the plant, or on lower leaves
  (no photo)

Cause:

* Caterpillars (see photo on the right)

Remedy:

* Find the caterpillar(s) and remove it. There may only be a few.

Other similar plant will have the same issue, like Cauliflower.

## Lettuce Problem 1:

{{< gallery dir="/pests-and-diseases/lettuce-1/images/" />}}

Observation:

* Leaves of the lettuce are sprinkled with small white spots
* Initially, the spots look like salt grains
* As they grow, they become identifiable as little insects (aphids?)
* Six legs under the microscope

Remedy:

* Spray with Neem oil
* Wash off dead pests

## Pepper Problem 1:

{{< gallery dir="/pests-and-diseases/pepper-1/images/" />}}

Observation:

* Leaves of the pepper plant have brown, dry spots

Cause:

* ???

Remedy:

* ???
