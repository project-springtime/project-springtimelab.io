#!/bin/bash
#
# Build all that needs building

MAKEFILES=$(find . -name Makefile)
for m in ${MAKEFILES}; do
    ( cd $(dirname $m) && pwd && make $@ )
done

IMAGES=$( find static/{blog,parts,pests-and-diseases,systems} -name \*.jpg -or -name \*.png -or -name \*.jpeg | grep -v thumb )
for f in ${IMAGES}; do
    ff=${f%.*}-thumb.${f##*.}
    echo Scaling $f
    magick convert $f -resize 245x245^ $ff
done

