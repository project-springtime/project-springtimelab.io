//
// Notes:
// There are two coordinate systems:
// * The SVG coordinate system, which determines things such as line width. It needs
//   to be driven by pixels, not design measures
// * The design measures, which could be mil or miles. They need to get mapped.
//

class DjsDiagram {
    constructor( elements, params ) {
        this.elements = elements;
        this.params   = params;
    }

    renderToSvg( svg ) {
        for( var i=0 ; i<this.elements.length ; ++i ) {
            var el = this.elements[i];
                if( el != null ) {
                    var children = el.convertToSvg( this.params );
                    if( children != null ) {
                      for( var j=0 ; j<children.length ; ++j ) {
                        var child = children[j];
                        if( child != null ) {
                            svg.appendChild( child );
                        }
                    }
                }
            }
        }
    }
}

// --

class DjsVector {
    constructor( dx, dy ) {
        this.dx = dx;
        this.dy = dy;
    }

    times( a ) {
        return new DjsVector( a * this.dx, a * this.dy );
    }

    norm() {
        return Math.sqrt( this.dx * this.dx + this.dy * this.dy );
    }

    normalized() {
        var norm = this.norm();
        return new DjsVector( this.dx / norm, this.dy / norm );
    }

    rotateByAngle( angle ) {
        var rad = angle/180*Math.PI;
        var cos = Math.cos( rad );
        var sin = Math.sin( rad );

        return new DjsVector( cos * this.dx - sin * this.dy, sin * this.dx + cos * this.dy );
    }

    flipAlongX() {
        return new DjsVector( this.dx, -this.dy );
    }

    equals( other ) {
        return this.dx == other.dx && this.dy == other.dy;
    }
}

// --

class DjsPoint {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    times( a ) {
        return new DjsPoint( a * this.x, a * this.y );
    }

    plus( dx, dy ) {
        return new DjsPoint( this.x + dx, this.y + dy );
    }

    plusVector( v ) {
        return new DjsPoint( this.x + v.dx, this.y + v.dy );
    }

    minusPoint( p ) {
        return new DjsVector( this.x - p.x, this.y - p.y );
    }

    rotateByAngle( center, angle ) {
        var rad = angle/180*Math.PI;
        var cos = Math.cos( rad );
        var sin = Math.sin( rad );
        var dx  = this.x - center.x;
        var dy  = this.y - center.y;

        return new DjsPoint( center.x + cos * dx - sin * dy, center.y + sin * dx + cos * dy );
    }

    equals( other ) {
        return this.x == other.x && this.y == other.y;
    }
}

// --

class DjsElement {
    convertToSvg( params ) {
        return null;
    }

    convertCoordinate( p, params ) {
        var originOffsetX = params["origin-offset-x"];
        var originOffsetY = params["origin-offset-y"];
        var scale  = params["scale"];
        var height = params["height"];

        return new DjsPoint( p.x * scale + originOffsetX, height - p.y * scale - originOffsetY );
    }

    convertVector( v, params ) {
        var scale  = params["scale"];

        return new DjsVector( v.dx * scale, - v.dy * scale );
    }

    distanceToLabel( d, params ) {
        var factor = Math.pow( 10, params["rounding"] );
        var ret    = d * factor;
        ret        = Math.round( ret );
        ret        = ret / factor;

        if( params["unit"] != null ) {
            ret += params["unit"];
        }
        return ret;
    }
}

// --

class DjsStep {
    constructor( end, hidden = false ) {
        this.end    = end;
        this.hidden = hidden;
    }

    convertToSvg( start, params ) {
        return null;
    }
}

class DjsLineStep extends DjsStep {
    constructor( end, hidden = false ) {
        super( end, hidden );
    }

    convertToSvg( edge, start, params ) {
        var startScaled = edge.convertCoordinate( start, params );
        var endScaled   = edge.convertCoordinate( this.end, params );

        // parallel line with two arrows
        var line = document.createElementNS( "http://www.w3.org/2000/svg", 'line');
        line.setAttributeNS( null, 'x1', startScaled.x );
        line.setAttributeNS( null, 'y1', startScaled.y );
        line.setAttributeNS( null, 'x2', endScaled.x );
        line.setAttributeNS( null, 'y2', endScaled.y );

        if( this.hidden ) {
            line.setAttributeNS( null, 'class', 'djs-part-hidden' );
        } else {
            line.setAttributeNS( null, 'class', 'djs-part' );
        }
        return [ line ];
    }
}

class DjsArcStep extends DjsStep {
    constructor( end, r, sweep, hidden = false ) {
        super( end, hidden );
        this.r     = r;
        this.sweep = sweep;
    }

    convertToSvg( edge, start, params ) {
        var startScaled = edge.convertCoordinate( start, params );
        var endScaled   = edge.convertCoordinate( this.end, params );
        var rScaled     = this.r * params["scale"];
        var sweep       = this.sweep ? "1" : "0";

        var d = "M " + startScaled.x + " " + startScaled.y; // start here
        // A rx ry x-axis-rotation large-arc-flag sweep-flag x y
        d += " A " + rScaled + " " + rScaled + ", 0, 0, " + sweep + ", " + endScaled.x + " " + endScaled.y;

        var path = document.createElementNS( "http://www.w3.org/2000/svg", 'path');
        path.setAttributeNS( null, 'd', d );
        if( this.hidden ) {
            path.setAttributeNS( null, 'class', 'djs-part-hidden' );
        } else {
            path.setAttributeNS( null, 'class', 'djs-part' );
        }
        return [ path ];
    }
}

class DjsClosedPathPart extends DjsElement {
    constructor( start, steps = [] ) {
        super();
        this.start = start;
        this.steps = steps;
    }

    lineTo( end ) {
        this.steps.push( new DjsLineStep( end ));
        return this;
    }

    hiddenLineTo( end ) {
        this.steps.push( new DjsLineStep( end, true ));
        return this;
    }

    arcTo( end, r, above = false ) {
        this.steps.push( new DjsArcStep( end, r, above ));
        return this;
    }

    hiddenArcTo( end, r, above = false ) {
        this.steps.push( new DjsArcStep( end, r, above, true ));
        return this;
    }

    convertToSvg( params ) {
        var ret   = [];
        var start = this.start;

        for( var i=0 ; i<this.steps.length ; ++i ) {
          ret = ret.concat( this.steps[i].convertToSvg( this, start, params ));
          start = this.steps[i].end;
        }
        if( !start.equals( this.start )) {
            // close edge
            ret = ret.concat( new DjsLineStep( start ).convertToSvg( this, this.start, params ));
        }
        return ret;
    }
}

class DjsRectanglePart extends DjsElement {
    constructor( start, end ) {
        super();
        this.start = start;
        this.end   = end;
    }

    convertToSvg( params ) {
        var startScaled = this.convertCoordinate( this.start, params );
        var endScaled   = this.convertCoordinate( this.end, params );
        var deltaScaled = this.convertVector( this.end.minusPoint( this.start ), params );

        var rect = document.createElementNS( "http://www.w3.org/2000/svg", 'rect');
        rect.setAttributeNS( null, 'class', 'djs-part' );
        rect.setAttributeNS( null, 'x', startScaled.x < endScaled.x ? startScaled.x : endScaled.x );
        rect.setAttributeNS( null, 'y', startScaled.y < endScaled.y ? startScaled.y : endScaled.y );
        rect.setAttributeNS( null, 'width', Math.abs( deltaScaled.dx ));
        rect.setAttributeNS( null, 'height', Math.abs( deltaScaled.dy ));

        return [ rect ];
    }
}

class DjsCirclePart extends DjsElement {
    constructor( center, radius ) {
        super();
        this.center = center;
        this.radius = radius;
    }

    convertToSvg( params ) {
        var scale  = params["scale"];

        var centerScaled = this.convertCoordinate( this.center, params );
        var radiusScaled = this.radius * scale;

        var rect = document.createElementNS( "http://www.w3.org/2000/svg", 'circle');
        rect.setAttributeNS( null, 'class', 'djs-part' );
        rect.setAttributeNS( null, 'cx', centerScaled.x );
        rect.setAttributeNS( null, 'cy', centerScaled.y );
        rect.setAttributeNS( null, 'r', radiusScaled );

        return [ rect ];
    }
}


// --

class DjsSymmetryLine extends DjsElement {
    constructor( start, end ) {
        super();

        this.start = start;
        this.end   = end;
    }

    convertToSvg( params ) {
        var startScaled = this.convertCoordinate( this.start, params );
        var endScaled   = this.convertCoordinate( this.end, params );

        var line = document.createElementNS( "http://www.w3.org/2000/svg", 'line');
        line.setAttributeNS( null, 'x1', startScaled.x );
        line.setAttributeNS( null, 'y1', startScaled.y );
        line.setAttributeNS( null, 'x2', endScaled.x );
        line.setAttributeNS( null, 'y2', endScaled.y );
        line.setAttributeNS( null, 'class', 'djs-symmetry' );
        return [ line ];
    }
}

// --

class DjsLinearMeasure extends DjsElement {
    constructor( p1, p2, above = 1, lineOffset = 40, overhang = 10 ) {
        super();

        this.p1 = p1;
        this.p2 = p2;
        this.above      = above;
        this.lineOffset = lineOffset;
        this.overhang   = overhang;
    }

    convertToSvg( params ) {
        var scale  = params["scale"];

        var distance         = this.p2.minusPoint( this.p1 );
        var orthoVector      = distance.rotateByAngle( this.above ? -90 : 90 ).flipAlongX().normalized();
        var overhangWithSign = this.lineOffset > 0 ? this.overhang : -this.overhang;

        var p1Scaled = this.convertCoordinate( this.p1, params );
        var p2Scaled = this.convertCoordinate( this.p2, params );

        var p1ScaledOrtho = p1Scaled.plusVector( orthoVector.times( this.lineOffset ) );
        var p2ScaledOrtho = p2Scaled.plusVector( orthoVector.times( this.lineOffset ) );

        var p1ScaledOrthoOver = p1Scaled.plusVector( orthoVector.times( this.lineOffset + overhangWithSign ) );
        var p2ScaledOrthoOver = p2Scaled.plusVector( orthoVector.times( this.lineOffset + overhangWithSign ) );

        // orthogonal lines
        var orthoLine1 = document.createElementNS( "http://www.w3.org/2000/svg", 'line');
        orthoLine1.setAttributeNS( null, 'x1', p1Scaled.x );
        orthoLine1.setAttributeNS( null, 'y1', p1Scaled.y );
        orthoLine1.setAttributeNS( null, 'x2', p1ScaledOrthoOver.x );
        orthoLine1.setAttributeNS( null, 'y2', p1ScaledOrthoOver.y );
        orthoLine1.setAttributeNS( null, 'class', 'djs-measure-ortho' );

        var orthoLine2 = document.createElementNS( "http://www.w3.org/2000/svg", 'line');
        orthoLine2.setAttributeNS( null, 'x1', p2Scaled.x );
        orthoLine2.setAttributeNS( null, 'y1', p2Scaled.y );
        orthoLine2.setAttributeNS( null, 'x2', p2ScaledOrthoOver.x );
        orthoLine2.setAttributeNS( null, 'y2', p2ScaledOrthoOver.y );
        orthoLine2.setAttributeNS( null, 'class', 'djs-measure-ortho' );

        // parallel line with two arrows
        var arrowLine = document.createElementNS( "http://www.w3.org/2000/svg", 'line');
        arrowLine.setAttributeNS( null, 'x1', p1ScaledOrtho.x );
        arrowLine.setAttributeNS( null, 'y1', p1ScaledOrtho.y );
        arrowLine.setAttributeNS( null, 'x2', p2ScaledOrtho.x );
        arrowLine.setAttributeNS( null, 'y2', p2ScaledOrtho.y );
        arrowLine.setAttributeNS( null, 'class', 'djs-measure-distance' );

        // label
        var label = document.createElementNS( "http://www.w3.org/2000/svg", 'text');
        label.setAttributeNS( null, "text-anchor", "middle" );
        label.setAttributeNS( null, 'x', ( p1ScaledOrtho.x + p2ScaledOrtho.x ) / 2 );
        label.setAttributeNS( null, 'y', ( p1ScaledOrtho.y + p2ScaledOrtho.y ) / 2 );
        label.innerHTML = this.distanceToLabel( distance.norm(), params );
        label.setAttributeNS( null, 'class', 'djs-measure-label' );

        return [ orthoLine1, orthoLine2, arrowLine, label ];
    }
}

class DjsRadialMeasure extends DjsElement {
    constructor( p, r, angle = 30 ) {
        super();

        this.p     = p;
        this.r     = r;
        this.angle = angle;
    }

    convertToSvg( params ) {
        var scale = params["scale"];
        var rad   = this.angle / 180 * Math.PI;

        var p2       = new DjsPoint( this.p.x + this.r * Math.cos( rad ), this.p.y + this.r * Math.sin( rad ) );
        var distance = p2.minusPoint( this.p );

        var p1Scaled = this.convertCoordinate( this.p, params );
        var p2Scaled = this.convertCoordinate( p2,     params );

        // radial line with arrow
        var arrowLine = document.createElementNS( "http://www.w3.org/2000/svg", 'line');
        arrowLine.setAttributeNS( null, 'x1', p1Scaled.x );
        arrowLine.setAttributeNS( null, 'y1', p1Scaled.y );
        arrowLine.setAttributeNS( null, 'x2', p2Scaled.x );
        arrowLine.setAttributeNS( null, 'y2', p2Scaled.y );
        arrowLine.setAttributeNS( null, 'class', 'djs-measure-radial' );

        // label
        var label = document.createElementNS( "http://www.w3.org/2000/svg", 'text');
        label.setAttributeNS( null, "text-anchor", "middle" );
        label.setAttributeNS( null, 'x', ( p1Scaled.x + p2Scaled.x ) / 2 );
        label.setAttributeNS( null, 'y', ( p1Scaled.y + p2Scaled.y ) / 2 );
        label.innerHTML = this.distanceToLabel( distance.norm(), params );
        label.setAttributeNS( null, 'class', 'djs-measure-label' );

        return [ arrowLine, label ];
    }
}

// --

class DjsPartLabel extends DjsElement {
    constructor( p, text, angle = 60, lineLength = 100 ) {
        super();

        this.p          = p;
        this.text       = text;
        this.angle      = angle;
        this.lineLength = lineLength;
    }

    convertToSvg( params ) {
        var rad = this.angle / 180 * Math.PI;
        var v   = new DjsVector( this.lineLength * Math.cos( rad ), - this.lineLength * Math.sin( rad ));

        var p1Scaled = this.convertCoordinate( this.p, params );
        var p2Scaled = p1Scaled.plusVector( v );

        // line
        var arrowLine = document.createElementNS( "http://www.w3.org/2000/svg", 'line');
        arrowLine.setAttributeNS( null, 'x1', p1Scaled.x );
        arrowLine.setAttributeNS( null, 'y1', p1Scaled.y );
        arrowLine.setAttributeNS( null, 'x2', p2Scaled.x );
        arrowLine.setAttributeNS( null, 'y2', p2Scaled.y );
        arrowLine.setAttributeNS( null, 'class', 'djs-part-label' );

        // label
        var label = document.createElementNS( "http://www.w3.org/2000/svg", 'text');
        label.setAttributeNS( null, "text-anchor", "middle" );
        label.setAttributeNS( null, 'x', p2Scaled.x );
        label.setAttributeNS( null, 'y', p2Scaled.y );
        label.innerHTML = this.text;
        label.setAttributeNS( null, 'class', 'djs-part-label' );

        return [ arrowLine, label ];
    }
}

// --

function djsRender( script, f ) {
    // find scale, viewBox in URL
    var scale;
    var scaleString   = script.getAttribute( "data-scale" );
    var width         = script.getAttribute( "data-width" );
    var height        = script.getAttribute( "data-height" );
    var rounding      = script.getAttribute( "data-rounding" );
    var unit          = script.getAttribute( "data-unit" );
    var originOffsetX = script.getAttribute( "data-origin-offset-x" );
    var originOffsetY = script.getAttribute( "data-origin-offset-y" );

    if( scaleString == null ) {
        scale = 100;
    } else {
        var slash       = scaleString.indexOf( "/" );
        if( slash > 0 ) {
          scale = parseFloat( scaleString.substr( 0, slash )) / parseFloat( scaleString.substr( slash+1 ));
        } else {
          scale = parseFloat( scaleString.substr( slash+1 ));
        }
    }
    if( width != null ) {
        width = parseInt( width );
    }
    if( width == null || width == 0 ) {
        width = 1000; // default
    }
    if( height != null ) {
        height = parseInt( height );
    }
    if( height == null || height == 0 ) {
        height = 1000; // default
    }

    if( originOffsetX != null ) {
        originOffsetX = parseInt( originOffsetX );
    }
    if( originOffsetX == null ) {
        originOffsetX = 0;
    }
    if( originOffsetY != null ) {
        originOffsetY = parseInt( originOffsetY );
    }
    if( originOffsetY == null ) {
        originOffsetY = 0;
    }

    var svgNode = document.createElementNS( "http://www.w3.org/2000/svg", "svg" );
    if( script.id != null && script.id != "" ) {
        svgNode.id  = script.id + "-svg";
    } else {
        svgNode.id = "drafting-js-" + Math.floor( Math.random() * 1000000) + "-svg";
    }
    svgNode.setAttribute( "viewBox", "0 0 " + width + " " + height );
    svgNode.innerHTML = `<defs>
 <marker id="arrow-start" markerWidth="20" markerHeight="14" refX="0" refY="6" orient="auto" markerUnits="strokeWidth">
  <path d="M18,0 L18,12 L0,6 z" fill="#0000ff"/>
 </marker>
 <marker id="arrow-end" markerWidth="20" markerHeight="14" refX="18" refY="6" orient="auto" markerUnits="strokeWidth">
  <path d="M0,0 L0,12 L18,6 z" fill="#0000ff"/>
 </marker>
</defs>`; // arrow-heads. Can't get fill="contextfill" to work.

    var params = {
        "scale"    : scale,
        "rounding" : rounding,
        "unit"     : unit,
        "width"    : width,
        "height"   : height,
        "origin-offset-x" : originOffsetX,
        "origin-offset-y" : originOffsetY
    };

    var diagram = f( params );
    diagram.renderToSvg( svgNode );

    script.after( svgNode );
}

