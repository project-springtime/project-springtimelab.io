
class ArmLeft extends DjsElement {
    constructor( start ) {
        super();
        this.start = start;
    }

    convertToSvg( params ) {
        var dx = 3;
        var r  = 3.5/2;
        var p  = this.start;

        var delegate = new DjsClosedPathPart( p )
                    .lineTo( p.plus( 10-3.5, 0 ))
                    .hiddenLineTo( p.plus( 10, 0 ))
                    .hiddenLineTo( p.plus( 10, 5 ))
                    .hiddenLineTo( p.plus( 10-3.5, 5 ))
                    .lineTo( p.plus( dx + r, 5 ))
                    .arcTo( p.plus( dx - r, 5 ), r, 1 )
                    .lineTo( p.plus( 0, 5 ));
        return delegate.convertToSvg( params );
    }
}

class ArmRight extends DjsElement {
    constructor( start ) {
        super();
        this.start = start;
    }

    convertToSvg( params ) {
        var dx = 10-3;
        var r  = 3.5/2;
        var p  = this.start;

        var delegate = new DjsClosedPathPart( p )
                    .lineTo( p.plus( 10, 0 ))
                    .lineTo( p.plus( 10, 5 ))
                    .lineTo( p.plus( dx + r, 5 ))
                    .arcTo( p.plus( dx - r, 5 ), r, 1 )
                    .lineTo( p.plus( 0, 5 ));
        return delegate.convertToSvg( params );
    }
}

djsRender( document.currentScript, function( params ) {
    var p  = new DjsPoint( 0, 0 );

    var yArmsLeft = [
        17,
        30,
        45,
        60
    ];
    var yArmsRight = [
        23,
        36,
        51,
        66
    ];

    var elements = [
        new DjsRectanglePart( p, p.plus( 3.5, 8 * 12 )),
        new DjsLinearMeasure( p, p.plus( 0, 8 * 12 ), 1, 500 ),
        new DjsPartLabel( p.plus( 2, 8 * 12 ), "Post-1" )
    ];

    for( var i=0 ; i<yArmsLeft.length ; ++i ) {
        var p1 = p.plus( -10 + 3.5, 0 );
        var p2 = p1.plus( 0, yArmsLeft[i] );
        elements.push( new ArmLeft( p2 ));
        elements.push( new DjsLinearMeasure( p1, p2, 0, 90 + i*40 ));
        elements.push( new DjsPartLabel( p2.plus( 0, 5 ), "Arm-1", 120 ));
    }
    for( var i=0 ; i<yArmsRight.length ; ++i ) {
        var p1 = p.plus( 10, 0 );
        var p2 = p1.plus( 0, yArmsRight[i] );
        elements.push( new ArmRight( p.plus( 0, yArmsRight[i] )));
        elements.push( new DjsLinearMeasure( p1, p2, 1, 100 + i*40 ));
        elements.push( new DjsPartLabel( p2.plus( 0, 5 ), "Arm-1" ));
    }

    return new DjsDiagram( elements, params );
} );
