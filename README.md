# Project Springtime

This is the repo for [Project Springtime](http://project-springtime.org/),
where we currently build, operate and document a vertical, backyard
hydroponics system. The content here uses the [Hugo](https://gohugo.io/)
static website generator.

Have bugs or improvements? File and issue or submit a pull request!

